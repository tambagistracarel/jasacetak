<?php

use Sabberworm\CSS\Comment\Comment;
use Sabberworm\CSS\Value\Value;

class Transaksi extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		checkOnLogin();
		// roleAkses();
		$this->load->model(array("ModelBank", "ModelPegawai", "ModelItemFormat", "ModelLog", "ModelBarang", "ModelOrder", "ModelBending"));
	}

	public function index()
	{
		$listTransaksi = $this->ModelOrder->getAll();
		$data = array(
			"header" => "Data Transaksi Super",
			"orders" => $listTransaksi,
			"page" => "contentSuper/Transaksi/v_list_transaksi"
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function register()
	{
		$data = array(
			"page" => "content/Pegawai/v_add_pegawai"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function detail_order($nomor)
	{
//		$noOrder = $this->input->post("no_order", true);
		$order = $this->ModelOrder->getByPrimaryKey($nomor);
		$itemFormat = $this->ModelItemFormat->getNoOrderById($nomor);
		$pegawai = $this->ModelPegawai->getAll();
		$bank = $this->ModelBank->getAll();
		$data = array(
			"header" => "Detail Order",
			"order" => $order,
			"formats" => $itemFormat,
			"pegawais" => $pegawai,
			"banks" => $bank,
			"page" => "contentSuper/Transaksi/v_detail_transaksi"
		);

		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function pembayaran($nomor)
	{
//		$noOrder = $this->input->post("no_order", true);
		$order = $this->ModelOrder->getByPrimaryKey($nomor);
		$bank = $this->ModelBank->getAll();
		$data = array(
			"header" => "Pembayaran",
			"order" => $order,
			"banks" => $bank,
			"page" => "contentSuper/Transaksi/v_pembayaran_transaksi"
		);

		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function riwayat_bayar($noOrder)
	{
		$log = $this->ModelLog->getNoOrderById($noOrder);
		$data = array(
			"header" => "Riwayat Pembayaran",
			"logs" => $log,
			"page" => "contentSuper/Transaksi/v_riwayat_transaksi"
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function update()
	{
		$data = array(
			"page" => "content/Pegawai/v_update_pegawai"
		);
		$this->load->view("layout/dashboard", $data);
	}

	public function proses_bayar()
	{
		$queryMaxId = "select ifnull(max(no_order),0) as max from kartu_order ";
		$max = $this->db->query($queryMaxId)->row()->max;
		$max = (int) $max;
		// "TRX/2020/04/0120"
		$strPad = str_pad($max + 1, 6, "0", STR_PAD_LEFT);
		$noPengantar = $strPad . date("/m/Y");

		$noOrder = $this->input->post("no_order", true);
		$customer = $this->input->post("nama_toko", true);
		$noPO = $this->input->post("no_po", true);
		$noPR = $this->input->post("no_pr", true);
		$jnsOrder = $this->input->post("jns_order", true);
		$jumlah = $this->input->post("buku", true);
		$jumlah = $this->input->post("set_buku", true);
		$jumlah = $this->input->post("eks", true);
		$hargaOrder = $this->input->post("harga_order", true);
		$totalSemua = $this->input->post("total_semua", true);
		$nomor = $this->input->post("nomor", true);
		$tampung_harga = $this->input->post("tampung_harga", true);
		$bayar = $this->input->post("bayar", true);
		$tampung_bayar = $this->input->post("tampung_bayar", true);
		$jnsBayar = $this->input->post("jns_bayar", true);
		$bank = $this->input->post("nama_bank", true);
		$rek = $this->input->post("no_rek", true);
		$atasNama = $this->input->post("atas_nama", true);
		$total = $this->input->post("total", true);
		$bayar = str_replace('.', '', $bayar);
		$tampung_harga = str_replace('.', '', $tampung_harga);
		$getTotal = $this->ModelOrder->getSubTotalById($noOrder);
		$getBayar = $this->ModelOrder->getSubBayarById($noOrder);

		if ($getTotal == $tampung_harga) {
			$hasil = $getTotal - $bayar;
		} else {
			$hasil = $getTotal - $bayar;
		}

		if($getBayar == '0'){
			$hasilByr = $bayar;
		}else if($getBayar == $bayar){
			$hasilByr = $bayar + $tampung_bayar;
		} else {
			$hasilByr = $getBayar + $bayar;
		}

		$orders = array(
			"nama_sales" => $namaSales,
			"no_pengantar" => $noPengantar,
			"tampung_harga" => $tampung_harga,
			"jns_bayar" => $jnsBayar,
			"nama_bank" => $bank,
			"no_rek" => $rek,
			"atas_nama" => $atasNama,
			"total" => $hasil,
			"bayar" => $bayar,
			"tampung_bayar" => $bayar,
			"tampung_bayar_1" => $hasilByr
		);

		helper_log($noOrder, $customer, $noPO, $noPR, $jnsOrder, $jumlah, $hargaOrder, $totalSemua, $bayar, $hasil, $nomor, $jnsBayar, $bank, $rek, $atasNama);
		$this->ModelOrder->update($noOrder, $orders);
		redirect("Transaksi");
	}

	public function proses_update()
	{
//		$id = $this->input->post("id_order", true);
		$noOrder = $this->input->post("no_order", true);
//		$nomor = $this->input->post("nomor", true);
		$namaSales = $this->input->post("nama_sales", true);
		$harga = $this->input->post("harga_order", true);
		$buku = $this->input->post("buku", true);
		$set = $this->input->post("set_buku", true);
		$eks = $this->input->post("eks", true);
		$ppn = $this->input->post("ppn", true);
		$diskon = $this->input->post("diskon", true);
		$harga = str_replace('.', '', $harga);

		
		$hasilppn = (($harga * $ppn) / 100);
		$hasilhargappn = $harga + $hasilppn;
		$hasildiskon = (($harga * $diskon) / 100);
		$total = $hasilhargappn - $hasildiskon;
//		$totalsemua = $total * $buku;
//		if ($buku){
//			$totalsemua = $total * $buku;
//		}else

		if ($buku == "" && $eks != "") {
			$totalsemua = $total * $set;
		} else if ($buku == "" && $set == "") {
			$totalsemua = $total * $eks;
		} else {
			$totalsemua = $total * $buku;
		}

		$orders = array(
			"nama_sales" => $namaSales,
			"harga_order" => $harga,
			"tampung_harga" => $total,
			"ppn" => $ppn,
			"diskon" => $diskon,
			"total" => $totalsemua,
			"total_semua" => $totalsemua,
		);

//		helper_log($noOrder, $namaSales, $harga, $piutang, $hasil, $nomor, $jnsBayar, $bank, $rek, $atasNama);
		$this->ModelOrder->update($noOrder, $orders);
//		$this->ModelRiwayatOrder->update($id, $orders);
		redirect("Transaksi");
	}

	function print_struk($nomor)
	{
		$order = $this->ModelOrder->getByPrimaryKey($nomor);
		$itemFormat = $this->ModelItemFormat->getNoOrderById($nomor);
//		$detail = $this->ModelOrder->getDetailTransaksi($noOrder);
		$data = array(
			"header" => " Print struk",
			"order" => $order,
			"format" => $itemFormat,
		);
		$html = $this->load->view('contentSuper/Transaksi/print/print_struk', $data, true);
		$this->fungsi->createPDF($html, 'KARTU ORDER', 'A5', 'portrait');
	}

	function print_struk_harga($nomor)
	{
		$order = $this->ModelOrder->getByPrimaryKey($nomor);
		$itemFormat = $this->ModelItemFormat->getNoOrderById($nomor);
//		$detail = $this->ModelOrder->getDetailTransaksi($noOrder);
		$data = array(
			"header" => " Print struk",
			"order" => $order,
			"format" => $itemFormat,
		);
		$html = $this->load->view('contentSuper/Transaksi/print/print_struk_harga', $data, true);
		$this->fungsi->createPDF($html, 'KARTU ORDER', 'A5', 'portrait');
	}
} 
