<?php

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // isLogin();
    }

    public function barang()
    {
        $this->load->model("modelBarang");
        $listBarang = $this->modelBarang->getAll()->result();
        $filename = "Daftar Barang";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:E1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Barcode Barang")
            ->setCellValue("C2", "Nama Barang")
            ->setCellValue("D2", "Harga Barang")
            ->setCellValue("E2", "Stock Barang");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:E2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        foreach ($listBarang as $barang) {
            $file->setActiveSheetIndex(0)
                ->setCellValue("A" . $baris, $no++)
                ->setCellValue("B" . $baris, $barang->barcode_barang)
                ->setCellValue("C" . $baris, $barang->nama_barang)
                ->setCellValue("D" . $baris, $barang->harga_barang)
                ->setCellValue("E" . $baris, $barang->stock_barang);
            $baris++;
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:E" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }

     public function customer()
    {
        $this->load->model("modelCustomer");
        $listCustomer = $this->modelCustomer->getAll();
        $filename = "Data customer";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:F1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Nama Customer")
            ->setCellValue("C2", "Kode Customer Besoft")
            ->setCellValue("D2", "Alamat")
            ->setCellValue("E2", "Kota")
            ->setCellValue("F2", "No NPWP");
        // ->setCellValue("E2", "Stock Barang");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:F2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        foreach ($listCustomer as $cs) {
            $file->setActiveSheetIndex(0)
                ->setCellValue("A" . $baris, $no++)
                ->setCellValue("B" . $baris, $cs->nama_toko)
                ->setCellValue("C" . $baris, $cs->kode_kustomer_besoft)
                ->setCellValue("D" . $baris, $cs->alamat_toko)
                ->setCellValue("E" . $baris, $cs->telepon_toko)
                ->setCellValue("F" . $baris, $cs->no_npwp);
            $baris++;
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:F" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }

    public function laporan_order_pertanggal()
    {
        $this->load->model("modelLaporan");
        $filename = "Daftar Order Pertanggal";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:E1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Nama Pembeli")
            ->setCellValue("C2", "Alamat")
            ->setCellValue("D2", "No Order")
            ->setCellValue("E2", "No PO")
            ->setCellValue("F2", "No PR")
            ->setCellValue("G2", "Tanggal Order")
            ->setCellValue("H2", "Jenis Order");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:E2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        $check_tgl = $this->uri->segment(3);
        // var_dump($check_tgl);die();
        if ($check_tgl == ''){
            $order_pertanggal = $this->modelLaporan->order_pertanggal();
            foreach ($order_pertanggal as $op) {
                $file->setActiveSheetIndex(0)
                    ->setCellValue("A" . $baris, $no++)
                    ->setCellValue("B" . $baris, $op->nama_toko)
                    ->setCellValue("C" . $baris, $op->alamat_toko)
                    ->setCellValue("D" . $baris, $op->no_order)
                    ->setCellValue("E" . $baris, $op->no_po)
                    ->setCellValue("F" . $baris, $op->no_pr)
                    ->setCellValue("G" . $baris, $op->tgl_order)
                    ->setCellValue("H" . $baris, $op->jns_order);
                $baris++;
            }
        }else{
            $order_pertanggal = $this->uri->segment(3);
            $pecah_tanggal = explode("_", $order_pertanggal);
            $filter_order_pertanggal = $this->modelLaporan->filter_order_pertanggal($pecah_tanggal);
            foreach ($filter_order_pertanggal as $op) {
                $file->setActiveSheetIndex(0)
                    ->setCellValue("A" . $baris, $no++)
                    ->setCellValue("B" . $baris, $op->nama_toko)
                    ->setCellValue("C" . $baris, $op->alamat_toko)
                    ->setCellValue("D" . $baris, $op->no_order)
                    ->setCellValue("E" . $baris, $op->no_po)
                    ->setCellValue("F" . $baris, $op->no_pr)
                    ->setCellValue("G" . $baris, $op->tgl_order)
                    ->setCellValue("H" . $baris, $op->jns_order);
                $baris++;
            }
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:H" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("G")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("H")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }

    public function laporan_pengiriman_pertanggal()
    {
        $this->load->model("modelLaporan");
        $filename = "Daftar Pengiriman Pertanggal";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:H1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Tanggal Pengiriman")
            ->setCellValue("C2", "Nama Pembeli")
            ->setCellValue("D2", "Alamat")
            ->setCellValue("E2", "No Order")
            ->setCellValue("F2", "No PO")
            ->setCellValue("G2", "No PR")
            ->setCellValue("H2", "Jenis Order");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:H2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        $check_tgl = $this->uri->segment(3);
        // var_dump($check_tgl);die();
        if ($check_tgl == ''){
            $pengiriman_pertanggal = $this->modelLaporan->pengiriman_pertanggal();
            foreach ($pengiriman_pertanggal as $op) {
                $file->setActiveSheetIndex(0)
                    ->setCellValue("A" . $baris, $no++)
                    ->setCellValue("B" . $baris, $op->tgl_minta_kirim)
                    ->setCellValue("C" . $baris, $op->nama_toko)
                    ->setCellValue("D" . $baris, $op->alamat_toko)
                    ->setCellValue("E" . $baris, $op->no_order)
                    ->setCellValue("F" . $baris, $op->no_po)
                    ->setCellValue("G" . $baris, $op->no_pr)
                    ->setCellValue("H" . $baris, $op->jns_order);
                $baris++;
            }
        }else{
            $pengiriman_pertanggal = $this->uri->segment(3);
            $pecah_tanggal = explode("_", $pengiriman_pertanggal);
            $filter_pengiriman_pertanggal = $this->modelLaporan->filter_pengiriman_pertanggal($pecah_tanggal);
            foreach ($filter_pengiriman_pertanggal as $op) {
                $file->setActiveSheetIndex(0)
                    ->setCellValue("A" . $baris, $no++)
                    ->setCellValue("B" . $baris, $op->tgl_minta_kirim)
                    ->setCellValue("C" . $baris, $op->nama_toko)
                    ->setCellValue("D" . $baris, $op->alamat_toko)
                    ->setCellValue("E" . $baris, $op->no_order)
                    ->setCellValue("F" . $baris, $op->no_po)
                    ->setCellValue("G" . $baris, $op->no_pr)
                    ->setCellValue("H" . $baris, $op->jns_order);
                $baris++;
            }
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:H" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("G")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("H")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }

    // public function export_detail_per_pemesan()
    // {

    //     if(isset($_POST['export_detail_per_pemesan'])){
    //         $data = "ayam";
    //         echo json_encode($data);
    //         $post = $this->input->post(null, true);
    //     $this->load->model("modelLaporan");
    //     $listPerPemesan = $this->modelLaporan->detail_per_pemesan($post);
    //     $filename = "Data Per Pemesan";
    //     //langkah pembuatan excel
    //     //1.Buat Objek dari Spreadsheet
    //     $file = new Spreadsheet;
    //     //2.Aktif di sheet tertentu sekalian buat header
    //     $file->setActiveSheetIndex(0)
    //         ->mergeCells("A1:B1")
    //         ->setCellValue("A1", $filename)
    //         ->setCellValue("A2", "No")
    //         ->setCellValue("B2", "Nama");
    //     // ->setCellValue("E2", "Stock Barang");
    //     //2.1 Set Kosmetik dari Excel
    //     $file->setActiveSheetIndex(0)
    //         ->getStyle("A1")
    //         ->applyFromArray($this->header());
    //     $file->setActiveSheetIndex(0)
    //         ->getStyle("A2:B2")
    //         ->applyFromArray($this->header());

    //     $baris = 3;
    //     $no = 1;
    //     foreach ($listPerPemesan as $cs) {
    //         $file->setActiveSheetIndex(0)
    //             ->setCellValue("A" . $baris, $no++)
    //             ->setCellValue("B" . $baris, $cs->nama_toko);
    //         $baris++;
    //     }
    //     //3.1 Kosmetik
    //     $file->setActiveSheetIndex(0)
    //         ->getStyle("A2:B" . --$baris)
    //         ->applyFromArray($this->borderStyle());
    //     //otomatis menyesuaikan lebar kolong
    //     $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
    //     $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
    //     $writer = new Xlsx($file);
    //     $filename = $filename . ".xlsx";
    //     header("Content-Type: application/vnd.ms-excel");
    //     header("Content-Disposition: attachment;filename=\"$filename\"");
    //     header("Cache-Control: max-age=0");
    //     $writer->save("php://output");

    //     }
    //     // $post = $this->input->post(null, true);
    //     // $this->load->model("modelLaporan");
    //     // $listPerPemesan = $this->modelLaporan->detail_per_pemesan($post);
    //     // $filename = "Data Per Pemesan";
    //     // //langkah pembuatan excel
    //     // //1.Buat Objek dari Spreadsheet
    //     // $file = new Spreadsheet;
    //     // //2.Aktif di sheet tertentu sekalian buat header
    //     // $file->setActiveSheetIndex(0)
    //     //     ->mergeCells("A1:B1")
    //     //     ->setCellValue("A1", $filename)
    //     //     ->setCellValue("A2", "No")
    //     //     ->setCellValue("B2", "Nama");
    //     // // ->setCellValue("E2", "Stock Barang");
    //     // //2.1 Set Kosmetik dari Excel
    //     // $file->setActiveSheetIndex(0)
    //     //     ->getStyle("A1")
    //     //     ->applyFromArray($this->header());
    //     // $file->setActiveSheetIndex(0)
    //     //     ->getStyle("A2:B2")
    //     //     ->applyFromArray($this->header());

    //     // $baris = 3;
    //     // $no = 1;
    //     // foreach ($listPerPemesan as $cs) {
    //     //     $file->setActiveSheetIndex(0)
    //     //         ->setCellValue("A" . $baris, $no++)
    //     //         ->setCellValue("B" . $baris, $cs->nama_toko);
    //     //     $baris++;
    //     // }
    //     // //3.1 Kosmetik
    //     // $file->setActiveSheetIndex(0)
    //     //     ->getStyle("A2:B" . --$baris)
    //     //     ->applyFromArray($this->borderStyle());
    //     // //otomatis menyesuaikan lebar kolong
    //     // $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
    //     // $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
    //     // $writer = new Xlsx($file);
    //     // $filename = $filename . ".xlsx";
    //     // header("Content-Type: application/vnd.ms-excel");
    //     // header("Content-Disposition: attachment;filename=\"$filename\"");
    //     // header("Cache-Control: max-age=0");
    //     // $writer->save("php://output");
    // }

    public function pegawai()
    {
        $this->load->model("modelPegawai");
        $listPegawai = $this->modelPegawai->getAll();
        $filename = "Data Pegawai";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:F1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Nama Anggota")
            ->setCellValue("C2", "Jabatan")
            ->setCellValue("D2", "Alamat")
            ->setCellValue("E2", "No Telepon")
            ->setCellValue("F2", "Keterangan");
        // ->setCellValue("E2", "Stock Barang");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:F2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        foreach ($listPegawai as $pegawai) {
            $file->setActiveSheetIndex(0)
                ->setCellValue("A" . $baris, $no++)
                ->setCellValue("B" . $baris, $pegawai->nama_anggota)
                ->setCellValue("C" . $baris, $pegawai->jabatan)
                ->setCellValue("D" . $baris, $pegawai->alamat_anggota)
                ->setCellValue("E" . $baris, $pegawai->telepon_anggota)
                ->setCellValue("F" . $baris, $pegawai->keterangan_anggota);
            $baris++;
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:F" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("F")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }

    public function kategori()
    {
        $this->load->model("modelKategori");
        $listKategori = $this->modelKategori->getAll();
        $filename = "Data Nama Kategori";
        //langkah pembuatan excel
        //1.Buat Objek dari Spreadsheet
        $file = new Spreadsheet;
        //2.Aktif di sheet tertentu sekalian buat header
        $file->setActiveSheetIndex(0)
            ->mergeCells("A1:E1")
            ->setCellValue("A1", $filename)
            ->setCellValue("A2", "No")
            ->setCellValue("B2", "Nama Kategori");
        //2.1 Set Kosmetik dari Excel
        $file->setActiveSheetIndex(0)
            ->getStyle("A1")
            ->applyFromArray($this->header());
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:E2")
            ->applyFromArray($this->header());

        //3. Load data dan populate ke excel
        $baris = 3;
        $no = 1;
        foreach ($listKategori as $kategori) {
            $file->setActiveSheetIndex(0)
                ->setCellValue("A" . $baris, $no++)
                ->setCellValue("B" . $baris, $kategori->nama_kategori);

            $baris++;
        }
        //3.1 Kosmetik
        $file->setActiveSheetIndex(0)
            ->getStyle("A2:E" . --$baris)
            ->applyFromArray($this->borderStyle());
        //otomatis menyesuaikan lebar kolong
        $file->setActiveSheetIndex(0)->getColumnDimension("A")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("B")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("C")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("D")->setAutoSize(true);
        $file->setActiveSheetIndex(0)->getColumnDimension("E")->setAutoSize(true);
        //4. Create Excel dan Download
        $writer = new Xlsx($file);
        $filename = $filename . ".xlsx";
        header("Content-Type: application/vnd.ms-excel");
        header("Content-Disposition: attachment;filename=\"$filename\"");
        header("Cache-Control: max-age=0");
        $writer->save("php://output");
    }


    private function borderStyle()
    {
        return array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
    }

    private function header()
    {
        return array(
            'font' => array(
                'bold' => true,
                'size' => 12
            ),
            'alignment' => array(
                'horizontal' => "center",
            ),
        );
    }

    private function setFont($size = 12, $bold = false, $alignment = "left")
    {
        return array(
            'font' => array(
                'bold' => $bold,
                'size' => $size
            ),
            'alignment' => array(
                'horizontal' => $alignment,
            ),
        );
    }

    private function dataAlign($align)
    {
        return array(
            'alignment' => array(
                'horizontal' => $align,
            ),
        );
    }
}
