<?php


class AppSuper extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array("ModelItemFormat", "AuthModel", "ModelNomerator", "ModelCustomer", "ModelJenisKertas", "ModelJenisOrder", "ModelPerforasi", "ModelPegawai", "ModelOrder", "ModelTransaksi", "ModelBending", "ModelItemTransaksi", "ModelFormat", "ModelUv"));
		// isLogin();
		checkOnLogin();
	}

	public function index()
	{
		$listCustomer = $this->ModelCustomer->getAll();
		$listPegawai = $this->ModelPegawai->getAll();
		$listBending = $this->ModelBending->getAll();
		$listFormat = $this->ModelFormat->getAll();
		$listFormatAll = $this->ModelItemFormat->getAll();
		$listOrder = $this->ModelOrder->getAll();
		$listUV = $this->ModelUv->getAll();
		$listNomerator = $this->ModelNomerator->getAll();
		$listJenisKertas = $this->ModelJenisKertas->getAll();
		$listJenisOrder = $this->ModelJenisOrder->getAll();
		$listPerforasi = $this->ModelPerforasi->getAll();
		$data = array(
			"header" => "App",
			"page" => "contentSuper/App/v_form_app",
			"customers" => $listCustomer,
			"bendings" => $listBending,
			"pegawais" => $listPegawai,
			"orders" => $listOrder,
			"formatss" => $listFormatAll,
			"uvs" => $listUV,
			"nomerators" => $listNomerator,
			"jnskertass" => $listJenisKertas,
			"jnsorders" => $listJenisOrder,
			"perforasis" => $listPerforasi,
			"formats" => $listFormat
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function proses_simpan()
	{
		$queryMaxId = "select ifnull(max(no_order),0) as max from kartu_order ";
		$max = $this->db->query($queryMaxId)->row()->max;
		$max = (int) $max;
		// "TRX/2020/04/0120"
		$strPad = str_pad($max + 1, 6, "0", STR_PAD_LEFT);
		$noOrder = $strPad . date("/m/Y");
		$paramsFormat = $this->input->post('dataFormat');
		$paramsCustomer = $this->input->post('dataCustomer');
		$data = array(
			"kode_kustomer" => $paramsCustomer['kode_kustomer'],
			"nama_toko" => $paramsCustomer['nama_toko'],
			"alamat_toko" => $paramsCustomer['alamat_toko'],
			"no_order" => $noOrder,
			"no_po" => $paramsCustomer['no_po'],
			"no_pr" => $paramsCustomer['no_pr'],
			"tgl_order" => $paramsCustomer['tgl_order'],
			"tgl_minta_kirim" => $paramsCustomer['tgl_minta_kirim'],
			"jns_order" => $paramsCustomer['jnsorder'],
			"rangkap" => $paramsCustomer['rangkap'],
			"buku" => $paramsCustomer['buku'],
			"set_buku" => $paramsCustomer['set_buku'],
			"eks" => $paramsCustomer['eks'],
			"kd_cabang" => $paramsCustomer['kd_cabang'],
			"nama_sales" => $paramsCustomer['nama_sales'],
			"jabatan" => $paramsCustomer['jabatan'],
			"offset_sablon_polos" => $paramsCustomer['offset_sablon_polos'],
			"perforasi" => $paramsCustomer['perforasi'],
			"nomerator" => $paramsCustomer['nomerator'],
			"nomerator_dua" => $paramsCustomer['nomerator_dua'],
			"nomerator_tiga" => $paramsCustomer['nomerator_tiga'],
			"warna_nomerator" => $paramsCustomer['warna_nomerator'],
			"bending" => $paramsCustomer['bending'],
			"uv_vernish_laminating" => $paramsCustomer['uv_vernish_laminating'],
			"foil" => $paramsCustomer['foil'],
			"degel" => $paramsCustomer['degel'],
			"catatan" => $paramsCustomer['catatan'],
			"nomor" => ($max + 1)
		);
		$this->ModelOrder->insert($data);
		$queryMaxNo = "select ifnull(max(no_order),0) as max from cet_table_format ";
		$maxNo = $this->db->query($queryMaxNo)->row()->max;
		$maxNo = (int) $maxNo;
		// "TRX/2020/04/0120"
		$strPad = str_pad($maxNo + 1, 6, "0", STR_PAD_LEFT);
		$noOrderFormat = $strPad . date("/m/Y");
		$cek = json_decode($paramsFormat);
		$datas = $cek->item_data;
		foreach ($datas as $item) {
			$dataSimpan[] = array(
				"no_order" => $noOrderFormat,
				"format" => $item->format,
				"jns_kertas" => $item->jns_kertas,
				"warna_kertas" => $item->warna_kertas,
				"warna_tinta" => $item->warna_tinta,
				// "custom" => $item->custom,
//				"jumlah_order" => $item->jumlah_order,
//				"harga_order" => $item->harga_order,
//				"subtotal" => $item->subtotal,
//				"total" => $item->total,
				"id_format" => $item->id_format,
				"nomor" => ($maxNo + 1)
			);
		}
		$this->ModelItemFormat->insertBatch($dataSimpan);
		if ($this->db->affected_rows() > 0) {
			$data = array("success" => true);
			redirect("Transaksi");
		} else {
			$data = array("success" => false);
			// redirect("Transaksi");
//			redirect('AppSuper');
		}
		// redirect("Transaksi");
		echo json_encode($data);
	}
}
