<?php
class Pegawai extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // checkNoLogin();
        // roleAkses();
        $this->load->model(array("AuthModel", "ModelPegawai", "ModelCabang"));
		if($this->AuthModel->is_role() != "superadmin"){
			redirect("auth");
		}
	}

    public function index()
    {
        $listPegawai = $this->ModelPegawai->getAll();
        $data = array(
            "page" => "contentSuper/Pegawai/v_list_pegawai",
            "header" => "Daftar Pegawai",
            "pegawais" => $listPegawai
        );
        $this->load->view("layoutSuper/dashboard", $data);
    }

    public function register()
    {
    	$listCabang = $this->ModelCabang->getAll();
        $data = array(
            "header" => "Tambah Data Pegawai",
            "cabangs" => $listCabang,
            "page" => "contentSuper/Pegawai/v_add_pegawai"
        );
        $this->load->view("layoutSuper/dashboard", $data);
    }

	public function proses_simpan()
	{
		$pegawai = array(
			"id_anggota" => $this->input->post('id_anggota'),
			"nama_anggota" => $this->input->post('nama_anggota'),
			"jabatan" => $this->input->post('jabatan'),
			"alamat_anggota" => $this->input->post('alamat_anggota'),
			"telepon_anggota" => $this->input->post('telepon_anggota'),
			"keterangan_anggota" => $this->input->post('keterangan_anggota')
		);
		$this->ModelPegawai->insert($pegawai);
		redirect('Pegawai');
	}


	public function update($id)
    {
        $listPegawai = $this->ModelPegawai->getByPrimaryKey($id);
        $data = array(
            "pegawais" => $listPegawai,
            "page" => "contentSuper/Pegawai/v_update_pegawai"
        );
        $this->load->view("layoutSuper/dashboard", $data);
    }

	public function proses_update()
	{
		$id = $this->input->post("id_anggota", true);
		$nama_anggota = $this->input->post("nama_anggota", true);
		$jabatan = $this->input->post("jabatan", true);
		$alamat_anggota = $this->input->post("alamat_anggota", true);
		$telepon_anggota = $this->input->post("telepon_anggota", true);
		$keterangan_anggota = $this->input->post("keterangan_anggota", true);
		$pegawais = array(
			"nama_anggota" => $nama_anggota,
			"jabatan" => $jabatan,
			"alamat_anggota" => $alamat_anggota,
			"telepon_anggota" => $telepon_anggota,
			"keterangan_anggota" => $keterangan_anggota
		);
		$this->ModelPegawai->update($id, $pegawais);
		redirect("Pegawai");
	}

	public function proses_hapus($id)
	{
		$this->ModelPegawai->delete($id);
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data Sukses dihapus');
		}
		redirect("Pegawai");
	}
}
