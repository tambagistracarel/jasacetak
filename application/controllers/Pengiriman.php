<?php


class Pengiriman extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		checkOnLogin();
		// roleAkses();
		$this->load->model(array("ModelLog","ModelOrder"));

	}

	public function index()
	{
		$listPengiriman = $this->ModelLog->getAll();
		$data = array(
			"page" => "contentSuper/Pengiriman/v_list_pengiriman",
			"header" => "Daftar Pengiriman",
			"kirims" => $listPengiriman
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	function strukPengiriman($no)
	{
		$order = $this->ModelOrder->getByPrimaryKey($no);
		$log = $this->ModelLog->getNoOrderById($no);
		$data = array(
			"header" => "Print struk",
			"pengiriman" => $log,
			"kirim" => $order
		);
		$html = $this->load->view('contentSuper/Pengiriman/print/struk_pengiriman', $data, true);
		$this->fungsi->createPDF($html, 'Data Pengiriman', 'A5', 'landscape');
	}

	function strukKwitansi($no)
	{
		$order = $this->ModelOrder->getByPrimaryKey($no);
		$log = $this->ModelLog->getNoOrderById($no);
		$data = array(
			"header" => "Print struk",
			"pengiriman" => $log,
			"kirim" => $order
		);
		$html = $this->load->view('contentSuper/Transaksi/print/kwitansi', $data, true);
		$this->fungsi->createPDF($html, 'Data Pengiriman', 'A5', 'landscape');
	}
}
