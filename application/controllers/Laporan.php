<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
	use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Laporan extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('modelCustomer');
		$this->load->model('ModelOrder');
		$this->load->model('modelPegawai');
		$this->load->model('modelLaporan');
	}

    public function index(){
        $data = array(
            "page" => "contentSuper/laporan/v_laporan",
            "header" => "Daftar Laporan",
        );
        $this->load->view("layoutSuper/dashboard", $data);
    }

    public function pilih_laporan(){
        $post = $this->input->post(null, true);
        $value_laporan = $this->uri->segment(3);
        $order_pertanggal = $this->uri->segment(4);
        $pengiriman_pertanggal = $this->uri->segment(4);
        $pembayaran_pertanggal = $this->uri->segment(4);
        if($value_laporan == "laporan_customer"){
            $customer = $this->modelCustomer->getAll();
            $data = array(
                "page" => "contentSuper/laporan/v_laporan",
                "header" => "Daftar Laporan",    
                "customer" => $customer, 
            );   
        }
        else if ($value_laporan == "laporan_order_pertanggal"){
            if($order_pertanggal == ""){
                $order_pertanggal = $this->modelLaporan->order_pertanggal();
                $data = array(
                    "page" => "contentSuper/laporan/v_laporan",
                    "header" => "Daftar Laporan",    
                    "order_pertanggal" => $order_pertanggal 
                );
            }else{
                $pecah_tanggal = explode("_", $order_pertanggal);
                // var_dump($pecah_tanggal[0]);die();        
                $order_pertanggal = $this->modelLaporan->filter_order_pertanggal($pecah_tanggal);
                $data = array(
                    "page" => "contentSuper/laporan/v_laporan",
                    "header" => "Daftar Laporan",    
                    "order_pertanggal" => $order_pertanggal 
                );
            }
        }
        else if ($value_laporan == "laporan_pengiriman_per_tanggal"){
            if($pengiriman_pertanggal == ""){
                $pengiriman_pertanggal = $this->modelLaporan->pengiriman_pertanggal();
                $data = array(
                    "page" => "contentSuper/laporan/v_laporan",
                    "header" => "Daftar Laporan",    
                    "pengiriman_pertanggal" => $pengiriman_pertanggal 
                );
            }else{
                $pecah_tanggal = explode("_", $pengiriman_pertanggal);
                // var_dump($pecah_tanggal[0]);die();        
                $pengiriman_pertanggal = $this->modelLaporan->filter_pengiriman_pertanggal($pecah_tanggal);
                $data = array(
                    "page" => "contentSuper/laporan/v_laporan",
                    "header" => "Daftar Laporan",    
                    "pengiriman_pertanggal" => $pengiriman_pertanggal 
                );
            }
        }
        else if($value_laporan == "laporan_per_pemesan"){
            $per_pemesan = $this->modelLaporan->per_pemesan();
            $data = array(
                "page" => "contentSuper/laporan/v_laporan",
                "header" => "Daftar Laporan",    
                "per_pemesan" => $per_pemesan, 
            );   
        }
        else if($value_laporan == "laporan_pembayaran_per_pemesan"){
            $pembayaran_per_pemesan = $this->modelLaporan->pembayaran_per_pemesan();
            $data = array(
                "page" => "contentSuper/laporan/v_laporan",
                "header" => "Daftar Laporan",    
                "pembayaran_per_pemesan" => $pembayaran_per_pemesan, 
            );   
        }
        else if($value_laporan == "laporan_pengiriman_per_no_order"){
            $pengiriman_no_order = $this->modelLaporan->pengiriman_no_order();
            $data = array(
                "page" => "contentSuper/laporan/v_laporan",
                "header" => "Daftar Laporan",    
                "pengiriman_no_order" => $pengiriman_no_order, 
            );   
        }
        else if($value_laporan == "laporan_pengiriman_per_pemesan"){
            $pengiriman_per_pemesan = $this->modelLaporan->per_pemesan();
            $data = array(
                "page" => "contentSuper/laporan/v_laporan",
                "header" => "Daftar Laporan",    
                "pengiriman_per_pemesan" => $pengiriman_per_pemesan, 
            );   
        }
        // else if ($value_laporan == "laporan_pembayaran_per_tanggal"){
        //     if($pembayaran_pertanggal == ""){
        //         $pembayaran_pertanggal = $this->modelLaporan->pembayaran_pertanggal();
        //         $data = array(
        //             "page" => "contentSuper/laporan/v_laporan",
        //             "header" => "Daftar Laporan",    
        //             "pembayaran_pertanggal" => $pembayaran_pertanggal 
        //         );
        //     }else{
        //         $pecah_tanggal = explode("_", $pembayaran_pertanggal);
        //         // var_dump($pecah_tanggal[0]);die();        
        //         $pembayaran_pertanggal = $this->modelLaporan->filter_pembayaran_pertanggal($pecah_tanggal);
        //         $data = array(
        //             "page" => "contentSuper/laporan/v_laporan",
        //             "header" => "Daftar Laporan",    
        //             "pembayaran_pertanggal" => $pembayaran_pertanggal 
        //         );
        //     }
        // }
        $this->load->view("layoutSuper/dashboard", $data);
    }	

    public function detail_per_pemesan(){
        $post = $this->input->post(null, true);
        if (isset($_POST['detail_pemesan'])){
            // echo "1";
            $data = $this->modelLaporan->detail_per_pemesan($post);
        }else{
            $data = "Data Tidak Ada";
        }
        echo json_encode($data);
    }

    public function detail_pengiriman_per_no_order(){
        $post = $this->input->post(null, true);
        if (isset($_POST['detail_pengiriman_per_no_order'])){
            // echo "1";
            $data = $this->modelLaporan->detail_pengiriman_per_no_order($post);
        }else{
            $data = "Data Tidak Ada";
        }
        echo json_encode($data);
    }

    public function detail_pengiriman_per_pemesan(){
        $post = $this->input->post(null, true);
        if (isset($_POST['detail_pengiriman_per_pemesan'])){
            // echo "1";
            $data = $this->modelLaporan->detail_pengiriman_per_pemesan($post);
        }else{
            $data = "Data Tidak Ada";
        }
        echo json_encode($data);
    }
    
    public function detail_pembayaran_per_pemesan(){
        $post = $this->input->post(null, true);
        if (isset($_POST['detail_pembayaran_per_pemesan'])){
            // echo "1";
            $data = $this->modelLaporan->detail_pembayaran_per_pemesan($post);
        }else{
            $data = "Data Tidak Ada";
        }
        echo json_encode($data);
    }

}

/* End of file Laporan.php */
/* Location: ./application/controllers/Laporan.php */

