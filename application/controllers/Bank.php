<?php


class Bank extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		checkOnLogin();
		// roleAkses();
		$this->load->model(array("ModelBank"));

	}

	public function index()
	{
		$listBank = $this->ModelBank->getAll();
		$data = array(
			"page" => "contentSuper/Bank/v_list_bank",
			"header" => "Daftar Bank",
			"banks" => $listBank
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function add()
	{
		$data = array(
			"header" => "Tambah Data Bank",
			"page" => "contentSuper/Bank/v_add_bank"
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function proses_simpan(){
		$bank = array(
			"id_bank" => $this->input->post('id_bank'),
			"nama_bank" => $this->input->post('nama_bank'),
			"no_rek" => $this->input->post('no_rek'),
			"atas_nama" => $this->input->post('atas_nama'),
		);
		$this->ModelBank->insert($bank);
		redirect('Bank');
	}

	public function update($id)
	{
		$listBank = $this->ModelBank->getByPrimaryKey($id);
		$data = array(
			"header" => "Ubah Data Bank",
			"banks" => $listBank,
			"page" => "contentSuper/Bank/v_update_bank"
		);
		$this->load->view("layoutSuper/dashboard", $data);
	}

	public function proses_update()
	{
		$id = $this->input->post("id_bank", true);
		$nama_bank = $this->input->post("nama_bank", true);
		$no_rek = $this->input->post("no_rek", true);
		$atas_nama = $this->input->post("atas_nama", true);
		$banks = array(
			"nama_bank" => $nama_bank,
			"no_rek" => $no_rek,
			"atas_nama" => $atas_nama,
		);
		$this->ModelBank->update($id, $banks);
		redirect("Bank");
	}

	public function proses_hapus($id)
	{
		$this->ModelBank->delete($id);
		if ($this->db->affected_rows() > 0) {
			$this->session->set_flashdata('success', 'Data Sukses dihapus');
		}
		redirect("Bank");
	}
}
