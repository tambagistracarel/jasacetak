<?php
function helper_log($noOrder, $customer, $po, $pr, $jenisOrder, $jumlah, $hargaSatuan, $total, $cicilan, $sisa, $nomor, $jnsBayar, $bank, $rek, $atasNama){
	$CI =& get_instance();

	// paramter
	$param['log_user']      = $CI->session->userdata('username');
	$param['log_no_order']      = $noOrder;
	$param['log_customer']      = $customer;
	$param['log_po']      = $po;
	$param['log_pr']      = $pr;
	$param['log_order']      = $jenisOrder;
	$param['log_jumlah']      = $jumlah;
	$param['log_harga_satuan']      = $hargaSatuan;
	$param['log_total']      = $total;
//	$param['log_harga']      = $total;
	$param['log_cicilan']      = $cicilan;
	$param['log_pembayaran']      = $sisa;
	$param['log_nomor']      = $nomor;
	$param['log_jns_bayar']      = $jnsBayar;
	$param['log_nama_bank']      = $bank;
	$param['log_no_rek']      = $rek;
	$param['log_atas_nama']      = $atasNama;

	//load model log
	$CI->load->model('ModelLog');

	//save to database
	$CI->ModelLog->save_log($param);

}
