<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>

			</div>
		</div>
	</div>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="card card-info">
					<div class="card-header">
						<h1>Ubah Data Bank</h1>
					</div>
					<div class="card-body">
						<form id="form-ubah-bank" method="post" action="<?= site_url('Bank/proses_update') ?>"
							  role="form">
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label>Nama Bank</label>
										<input type="text" class="form-control form-control-sm" id="nama_bank"
											   name="nama_bank" value="<?= $banks->nama_bank ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>No. Rekening</label>
										<input type="number" min="1" class="form-control form-control-sm" id="no_rek"
											   name="no_rek" value="<?= $banks->no_rek ?>" placeholder="Enter ..."
											   required>
									</div>
									<div class="form-group">
										<label>Atas Nama</label>
										<input type="text" class="form-control form-control-sm" id="atas_nama"
											   name="atas_nama" value="<?= $banks->atas_nama ?>" placeholder="Enter ..."
											   required>
									</div>
								</div>
							</div>
							<div class="card-footer">
								<button id="btn-save" class="btn btn-sm btn-success"><i class="fas fa-edit"></i>Ubah
								</button>
							</div>
							<input type="hidden" id="id_bank" name="id_bank"
								   value="<?= $banks->id_bank; ?>"/>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--  -->
</div>
<script>
	$(function () {
		$("#btn-save").on("click", function () {
			let validate = $("#form-ubah-bank").valid();
			if (validate) {
				Swal.fire({
					icon: 'success',
					title: 'Selamat',
					text: ' Data Bank Anda telah diubah',
				});
				$("#form-ubah-bank").submit();
			}
		});
		$("#form-ubah-bank").validate({
			rules: {
				nomer: {
					digits: true
				},
				alamat: {
					required: true
				}
			},
			messages: {
				kode: {
					digits: "Hanya nomer saja"
				}
			},
			errorElement: 'span',
			errorPlacement: function (error, element) {
				error.addClass('invalid-feedback');
				element.closest('.form-group').append(error);
			},
			highlight: function (element, errorClass, validClass) {
				$(element).addClass('is-invalid');
			},
			unhighlight: function (element, errorClass, validClass) {
				$(element).removeClass('is-invalid');
			}
		});
	});
</script>
