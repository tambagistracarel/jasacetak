<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h1>Tambah Data Bank</h1>
                    </div>
                    <div class="card-body">
                        <form id="form-tambah-bank" method="post" action="<?= site_url('Bank/proses_simpan') ?>" role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Nama Bank</label>
                                        <input type="text" class="form-control form-control-sm" id="nama_bank" name="nama_bank" placeholder="" required>
                                    </div>
                                    <div class="form-group">
                                        <label>No. Rekening</label>
                                        <input type="number" min="1" class="form-control form-control-sm" id="no_rek" name="no_rek" placeholder="" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Atas Nama</label>
                                        <input type="text" class="form-control form-control-sm" id="atas_nama" name="atas_nama" placeholder="" required>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer">
                        <button id="btn-save-bank" type="button" class="btn btn-success"><i class="fas fa-file-export"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $(function() {
        $("#btn-save-bank").on("click", function() {
            let validate = $("#form-tambah-bank").valid();
            if (validate) {
                Swal.fire({
								icon: 'success',
								title: 'Selamat',
								text: ' Data Bank Anda telah disimpan',
							});
                $("#form-tambah-bank").submit();
            }
        });
        $("#form-tambah-bank").validate({
            rules: {
                nomer: {
                    digits: true
                },
                alamat: {
                    required: true
                }
            },
            messages: {
                kode: {
                    digits: "Hanya nomer saja"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
