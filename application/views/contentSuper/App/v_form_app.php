<div class="content-wrapper">
	<section class="content">
		<form enctype="multipart/form-data" method="post" action="<?= site_url("AppSuper/proses_simpan"); ?>"
			  id="formDataCust">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h5>Pilih Customer</h5>
						</div>
						<div class="card-body">
							<div class="col-md-5">
								<div class="form-group">
									<label for="">Pilih Customer</label>
									<select class="form-control" id="select-customer" required>
										<option value="" disabled selected>Pilih Customer</option>
										<?php
										foreach ($customers as $c) {
											echo "<option data-kode='$c->kd_kuskategori' "
													. "data-nama='$c->nama_toko' "
													. "data-alamat='$c->alamat_toko' "
													. "value='$c->id_kustomer'> "
													. "$c->kd_kuskategori / $c->nama_toko"
													. "</option>";
										}
										?>
									</select>
								</div>
							</div>
							<div class="row">
								<input type="hidden" id="no-order" name="no_order" class="form-control" required/>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">NO PO</label>
										<input placeholder="INPUTAN HARUS ANGKA" type="number" min="1" id="no-po"
											   name="no_po" class="form-control"
											   required/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">NO PR</label>
										<input placeholder="INPUTAN HARUS ANGKA" type="number" min="1" id="no-pr" name="no_pr" class="form-control"
											   required/>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="">Kode Customer</label>
								<input readonly type="text" id="kode-kustomer" name="kode_kustomer" class="form-control"
									   required/>
							</div>
							<div class="form-group">
								<label for="">Nama Customer</label>
								<input readonly type="text" id="nama-toko" name="nama_toko" class="form-control"
									   required/>
							</div>
							<div class="form-group">
								<label for="">Alamat</label>
								<input readonly type="text" id="alamat-toko" name="alamat_toko" class="form-control"
									   required/>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									<label for="">Tanggal Order</label>
									<input type="date" id="tanggal-order" name="tgl_order"
										   value="<?php echo date("d-m-Y"); ?>" class="form-control"
										   required/>
								</div>
								<div class="form-group">
									<label for="">Tanggal Minta Dikirim</label>
									<input type="date" id="tanggal-minta-dikirim" <?php echo date("d-m-Y"); ?>
										   name="tgl_minta_kirim"
										   class="form-control" required/>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12">
					<div class="card">
						<div class="card-header">
							<h5>Pilih Sales Order</h5>
						</div>
						<div class="card-body">
							<div class="form-group">
								<label for="">Pilih Sales</label>
								<select name="" class="form-control" id="select-sales" required>
									<option value="" disabled selected>Pilih Sales</option>
									<?php
									foreach ($pegawais as $c) {
										echo "<option data-kode='$c->kd_cabang' "
												. "data-nama='$c->nama_anggota' "
												. "data-jabatan='$c->jabatan' "
												. "value='$c->id_anggota'> "
												. "$c->kd_cabang / $c->nama_anggota"
												. "</option>";
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label for="">Kode Cabang</label>
								<input readonly type="text" id="kode-cabang" name="kd_cabang" class="form-control"/>
							</div>
							<div class="form-group">
								<label for="">Nama Sales</label>
								<input readonly type="text" id="nama-sales" name="nama_sales" class="form-control"/>
							</div>
							<div class="form-group">
								<label for="">Jabatan</label>
								<input readonly type="text" id="jabatan" name="jabatan" class="form-control"/>
							</div>
						</div>
					</div>
					<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label for="">Jenis Order</label>
								<select name="jnsorder" class="form-control" id="select-jnsorder">
									<option value="" selected disabled>Pilih Jenis Order</option>
									<?php
									foreach ($jnsorders as $j) { ?>
										<option value="<?= $j->nama_jnsorder ?>"><?= $j->nama_jnsorder ?></option>
									<?php }
									?>
								</select>
							</div>
						</div>
					</div>
					<!-- </div> -->
					<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<h5>Pilih Format</h5>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Format</label>
								<input type="text" id="format" name="format"
									   onkeyup="this.value = this.value.toUpperCase()" class="form-control" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Jenis Kertas</label>
								<select name="jns_kertas" class="form-control" id="select-jnskertas" >
									<option value="" selected disabled>Pilih Jenis Kertas</option>
									<?php
									foreach ($jnskertass as $f) { ?>
										<option data-jnskertas="<?= $f->nama_jnskertas ?>"><?= $f->nama_jnskertas ?></option>
									<?php }
									?>
								</select>
							</div>
						</div>
					</div>
					<!--" class="form-control"/>-->
<!--					<input type="hidden" id="format" name="format" class="form-control"/>-->
					<input type="hidden" id="jns-kertas" name="jns_kertas" class="form-control"/>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Warna Kertas</label>
								<input type="text" id="warna-kertas" name="warna_kertas"
									   onkeyup="this.value = this.value.toUpperCase()" class="form-control" />
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Warna Tinta</label>
								<input type="text" id="warna-tinta" onkeyup="this.value = this.value.toUpperCase()"
									   name="warna_tinta" class="form-control" />
							</div>
						</div>
					</div>
					<div class="row">
						<div class="card-body">
							<div class="form-group">
								<button type="button" id="btn-add-item" class="btn btn-primary float-left">
									<i class="fas fa-plus"></i> Tambah
								</button>
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<input type="hidden" id="wajib_isi" name="wajib_isi">
					<table id="table-item" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th>Format</th>
							<th>Jenis Kertas</th>
							<th>Warna Kertas</th>
							<th>Warna Tinta</th>
							<th>Action</th>
						</tr>
						</thead>
						<tbody>

						</tbody>
					</table>
				</div>
			</div>
		</div>
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Rangkap</label>
										<input readonly type="text" id="rangkap" name="rangkap" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Buku</label>
										<input type="number" min="1" id="buku" name="buku" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Set</label>
										<input type="number" min="1" id="set" name="set_buku" class="form-control"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Eks</label>
										<input type="number" min="1" id="eks" name="eks" class="form-control"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
				<!-- <div class="card">
						<div class="card-body">
							<div class="form-group">
								<label for="">Jenis Order</label>
								<select name="jnsorder" class="form-control" id="select-jnsorder">
									<option value="" selected disabled>Pilih Jenis Order</option>
									<?php
									foreach ($jnsorders as $j) { ?>
										<option value="<?= $j->nama_jnsorder ?>"><?= $j->nama_jnsorder ?></option>
									<?php }
									?>
								</select>
							</div>
						</div>
					</div> -->
					<div class="card">
						<div class="card-header">
							<h5>Spesifikasi</h5>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Offset/Sablon/Polos</label>
										<select name="offset_sablon_polos" class="form-control" id="select-offset">
											<option value="" selected disabled>Pilih Offset/Sablon/Polos</option>
											<option value="Offset">Offset</option>
											<option value="Sablon">Sablon</option>
											<option value="Polos">Polos</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Perforasi</label>
										<select name="perforasi" class="form-control" id="select-perforasi">
											<option value="" selected disabled>Pilih Perforasi</option>
											<?php
											foreach ($perforasis as $f) { ?>
												<option value="<?= $f->perforasi ?>"><?= $f->perforasi ?></option>
											<?php }
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Nomerator</label>
										<div class="row">
											<div class="col-1">
												<input type="radio" class="form-radio" name="nomerator_tiga" value="Y"/>Ya
											</div>
											<div class="col-3">
												<input type="text" id="nomerator" name="nomerator"
													   class="form-control"/>
											</div>
											<div class="text-center col-1">
												s/d
											</div>
											<div class="col-3">
												<input type="text" id="nomerator_dua" name="nomerator_dua"
													   class="form-control"/>
											</div>
											<div class="col-3">
												<input type="radio" class="form-radio" name="nomerator_tiga" value="T"/>Tidak
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Warna Nomerator</label>
										<select name="warna_nomerator" class="form-control" id="select-warna-nomerator">
											<option value="" selected disabled>Pilih Warna Nomerator</option>
											<option value="MERAH">MERAH</option>
											<option value="HIJAU">HIJAU</option>
											<option value="BIRU">BIRU</option>
											<option value="HITAM">HITAM</option>
											<option value="PUTIH">PUTIH</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Bending</label>
										<select name="bending" class="form-control" id="select-bending">
											<option value="" selected disabled>Pilih Bending</option>
											<?php
											foreach ($bendings as $f) { ?>
												<option value="<?= $f->bending ?>"><?= $f->bending ?></option>
											<?php }
											?>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">UV/Vernish/Laminating/Tidak</label>
										<select name="uv_vernish_laminating" class="form-control" id="select-uv">
											<option value="" selected disabled>Pilih UV/Vernish/Laminating/Tidak
											</option>
											<?php
											foreach ($uvs as $f) { ?>
												<option value="<?= $f->uv_vernis_laminasi ?>"><?= $f->uv_vernis_laminasi ?></option>
											<?php }
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Foil</label><br>
										<input type="radio" class="form-radio" name="foil" value="Ya"/>&nbsp;&nbsp;Ya
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" class="form-radio" name="foil" value="Tidak"/>&nbsp;&nbsp;TIdak
										<br><br>
										<label for="">Degel</label><br>
										<input type="radio" class="form-radio" name="degel" value="Ya"/>&nbsp;&nbsp;Ya
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<input type="radio" class="form-radio" name="degel" value="Tidak"/>&nbsp;&nbsp;Tidak
									</div>
								</div>
								<input type="hidden" id="id-order" name="id_order" class="form-control"/>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Catatan</label><br>
										<textarea type="text" class="form-control" name="catatan" value=""
												  rows="4"></textarea>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					
				</div>
			</div>
			<button style="display:none" type="submit" id="btn-save-transaksi-hidden" class="btn btn-primary float-right"><i
						class="fas fa-save"></i>SIMPAN
			</button>
		</form>
		
		<div class="card-body">
			<button type="button" id="btn-save-transaksi" class="btn btn-primary float-right"><i
						class="fas fa-save"></i>SIMPAN
			</button>
		</div>
	</section>
</div>
<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>
<script>
	$(document).ready(function () {
		var now = new Date();
		var month = (now.getMonth() + 1);
		var day = now.getDate();
		if (month < 10)
			month = "0" + month;
		if (day < 10)
			day = "0" + day;
		var today = now.getFullYear() + '-' + month + '-' + day;
		$('#tanggal-order').val(today);
		$('#tanggal-minta-dikirim').val(today);
	});
	
	$(function () {
		$("#select-customer")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#kode-kustomer").val(optionSelected.data("kode"));
					$("#nama-toko").val(optionSelected.data("nama"));
					$("#alamat-toko").val(optionSelected.data("alamat"));
					$("#jumlah-barang").val(1);
				});
		$("#btn-save-barang").on("click", function () {
			let validate = $("#form-tambah-barang").valid();
			if (validate) {
				$("#form-tambah-barang").submit();
			}
		});
		$("#select-sales")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#kode-cabang").val(optionSelected.data("kode"));
					$("#nama-sales").val(optionSelected.data("nama"));
					$("#jabatan").val(optionSelected.data("jabatan"));
					$("#jumlah-barang").val(1);
				});
		$("#select-format")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#format").val(optionSelected.data("format"));
					$("#warna-tinta").val(optionSelected.data("tinta"));
					$("#warna-kertas").val(optionSelected.data("kertas"));
					$("#jumlah-order").val(1);
				});
		$("#select-jnskertas")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#jns-kertas").val(optionSelected.data("jnskertas"));
				});
		$("#select-bending").select2()
		$("#select-offset").select2()
		$("#select-perforasi").select2()
		$("#select-uv").select2()
		$("#select-warna-nomerator").select2()
		$("#select-jnsorder").select2()

		// $('#harga').mask('000.000.000.000.000.000', {reverse: true});

		$("#btn-add-item").on("click", function () {
			let id = $("#select-format").val();
			// let noOrder = $("#no-order").val();
			let format = $("#format").val();
			let jenisKertas = $("#jns-kertas").val();
			let warnaKertas = $("#warna-kertas").val();
			let warnaTinta = $("#warna-tinta").val();
			let custom = $("#custom").val();
			// let jumlahOrder = $("#jumlah-order").val();
			// let harga = $("#harga").val();
			// let total = parseInt(harga) * parseInt(jumlahOrder);
			// let subTotal = 0;
			// let subTotal += parseInt($(total).find(total).text());
			$.LoadingOverlay("show");
			if (format != "") {
				let tr = `<tr data-id="${id}">`;
				// tr += `<td>${noOrder}</td>`;
				tr += `<td>${format}</td>`;
				tr += `<td>${jenisKertas}</td>`;
				tr += `<td>${warnaKertas}</td>`;
				tr += `<td>${warnaTinta}</td>`;
				// tr += `<td>${custom}</td>`;
				// tr += `<td>${jumlahOrder}</td>`;
				// tr += `<td>${harga}</td>`;
				// tr += `<td id="total">${total}</td>`;
				// tr += `<td type="hidden">${subTotal}</td>`;
				tr += `<td>`;
				tr += `<button class="btn btn-xs btn-del-item btn-danger"> <i class="fas fa-trash"></i></button>`;
				tr += `</td>`;
				tr += `</tr>`;

				// $("#table-item tbody").append(function() {
				// 	subTotal += parseInt($(this).find("#total").text())
				// })
				$("#table-item tbody").append(tr);
				$("#select-format").val("").trigger("change");
				$("#select-jnskertas").val("").trigger("change");
				$("#format").val("").trigger("change");
				$("#jns-kertas").val();
				$("#warna-kertas").val("").trigger("change");
				$("#warna-tinta").val("").trigger("change");
				// $("#custom").val("").trigger("change");
				$("#jumlah-order").val("").trigger("change");
				$(".btn-del-item").on("click", function () {
					$(this).parent().parent().remove();
				});
				$.LoadingOverlay("hide");
				$("#wajib_isi").val("ok")
				var jmlRow = $("#table-item tbody tr").length;
				$("#rangkap").val(jmlRow);
			}
		});

		$("#formDataCust").submit(function (e) {
			e.preventDefault();

			var dataString = form_to_json("#formDataCust");

			var tes = $("#wajib_isi").val();
			if (tes != "ok") {
				Swal.fire({
					icon: 'error',
					title: 'warning',
					text: 'belum diisi',
				});
			} else {
				// Swal.fire({
				// 	icon: 'success',
				// 	title: 'Selamat',
				// 	text: 'sudah diisi',
				// });

				var data = {};
				data.item_data = [];
				let rows = $("#table-item tbody tr");
				rows.each(function () {
					let item = {};
					item.id_format = $(this).attr("data-id");
					item.format = $(this).children().eq(0).html();
					item.jns_kertas = $(this).children().eq(1).html();
					item.warna_kertas = $(this).children().eq(2).html();
					item.warna_tinta = $(this).children().eq(3).html();
					// item.custom = $(this).children().eq(4).html();
					// item.jumlah_order = $(this).children().eq(5).html();
					// item.harga_order = $(this).children().eq(6).html();
					// item.subtotal = $(this).children().eq(7).html();
					// item.total = $(this).children().eq(8).html();

					data.item_data.push(item);
				});

				var dataKirim = JSON.stringify(data);

				// $.LoadingOverlay("show");

				$.ajax({
					url: '<?= base_url() ?>' + 'AppSuper/proses_simpan',
					type: "POST",
					data: {

						dataCustomer: dataString,
						dataFormat: dataKirim
					},
					success: function (result) {
						if (result.success == true) {
							//success

						} else {
							// //error
							Swal.fire({
									icon: 'success',
									title: 'Selamat',
									text: 'Proses Transaksi Anda Berhasil',
								});
							window.location.replace('<?= site_url() ?>' + 'AppSuper');
						}
						// $.LoadingOverlay("hide");
					}
				});
			}
		})

		function form_to_json(selector) {
			var ary = $(selector).serializeArray();
			var obj = {};
			for (var a = 0; a < ary.length; a++) obj[ary[a].name] = ary[a].value;
			return obj;
		}
	});
</script>
<script type="text/javascript">
	$("#btn-save-transaksi").click(function(){
		$("#btn-save-transaksi-hidden").trigger("click");

	})
</script>
