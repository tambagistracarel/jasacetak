<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>
				<div class="col-sm-6">

				</div>
			</div>
		</div>
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-header">
				<h2>Data Pengiriman</h2>
			</div>
			<div class="card-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th style="text-align:center">No.</th>
						<th style="text-align:center">Tanggal/Waktu</th>
						<th style="text-align:center">No. Order</th>
						<th style="text-align:center">Nama Pemesan</th>
						<th style="text-align:center">No. PO</th>
						<th style="text-align:center">No. PR</th>
						<th style="text-align:center">Jenis Order</th>
						<th style="text-align:center">Jumlah</th>
						<th style="text-align:center">Harga Satuan</th>
						<th style="text-align:center">Total</th>
						<th style="text-align:center">Action</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					foreach ($kirims as $L) {
						?>
						<tr>
							<td style="text-align:center"><?= $no++ ?></td>
							<td style="text-align:center"><?= $L->log_time ?></td>
							<td style="text-align:center"><?= $L->log_no_order ?></td>
							<td style="text-align:center"><?= $L->log_customer ?></td>
							<td style="text-align:center"><?= $L->log_po ?></td>
							<td style="text-align:center"><?= $L->log_pr ?></td>
							<td style="text-align:center"><?= $L->log_order ?></td>
							<td style="text-align:center"><?= $L->log_jumlah ?></td>
							<td style="text-align:center"><?= formatRupiah($L->log_harga_satuan) ?></td>
							<td style="text-align:center"><?= formatRupiah($L->log_total) ?></td>
							<td style="text-align:center">
									<a href="<?= site_url('Pengiriman/strukPengiriman/') . $L->log_nomor ?>"
									   class="btn btn-sm bg-gradient-fuchsia">
										<i class="fas fa-shopping-cart"></i></a>
                            </td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
