<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Print Struck Harga</title>
	<style type="text/css">
		html {
			font-family: "Verdana, Arial";
		}

		.content {
			width: 100%;
			font-size: 10px;
			/* padding: 5px; */
			border: 1px solid black;
		}

		.title {
			text-align: center;
			font-size: 10px;
			padding-bottom: 8px;
			border-bottom: 1px solid;
		}

		.head {
			margin-top: 5px;
			margin-bottom: 10px;
			padding-bottom: 10px;
			border-bottom: 1px solid;
		}

		.padding, .nota1, .nota2 {
		  border: 1px solid black;
		}

		.padding {
		  width: 100%;
		  border-collapse: collapse;
		  font-size: 10px;
		}

		.table {
			width: 100%;
			font-size: 10px;
			
		}

		.thanks {
			/* margin-top: 10px; */
			padding-top: 12px;
			padding-bottom: 8px;
			text-align: center;
			font-size: 10px;
			border-top: 1px solid;
		}

		@media print {
			@page {
				width: 90mm;
				margin: 0mm
			}
		}

		@media all {
		.page-break { display: none; }
		}

		@media print {
		.page-break { display: block; page-break-before: always; }
		}
	</style>
</head>

<?php 
	$jml_array = COUNT($pengiriman);
	$jml_halaman = $jml_array / 5;
	$jml_halaman = ceil($jml_halaman);
?>

<body onload="window.print()">

<?php $no = 1 ?>
<?php $mulai_data = 0; ?>
<?php for ($i=0; $i < $jml_halaman; $i++) { ?>

<div class="content ">
	<div class="title">
	<?= $jml_array ?>
    <b>Percetakan dan Penerbitan</b>
		<b>CV.Andi Offset</b>
		<br>
		JL. BEO 38 - 40 TELP (0274) 561881
	</div>
	<div class="title">
		<b>SURAT PENGANTAR</b>
	</div>
	<div>
		<table cellspacing="3" cellpadding="3">
			<tr>
				<td>Yogyakarta, <?= $kirim->tgl_minta_kirim ?></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No : <?= $kirim->no_pengantar ?></td>
			</tr>

			<tr>
				<td>Kepada Yth : <?= $kirim->nama_toko ?><br><br><br><br></td>
			</tr>
			<tr>
				<td>Dengan hormat,<br></td>
			</tr>
			<tr>
				<td>Mohon agar diterima barang-barang cetakan sebagai berikut :</td>
			</tr>
		</table>
	</div>
	<div>
		<table class="padding">
			<thead>
			<tr>
				<th class="nota1" style="text-align:left">NO ORDER</th>
				<th class="nota1" style="text-align:left">NO PO</th>
				<th class="nota1" style="text-align:left">NO PR</th>
				<th class="nota1" style="text-align:left">NAMA ORDER</th>
				<th  class="nota1" style="text-align:left">JUMLAH</th>
				<th class="nota1" style="text-align:left">HARGA SATUAN</th>
				<th class="nota1" style="text-align:left">TOTAL HARGA</th>
				<th class="nota1" style="text-align:center">CATATAN</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$peritem = 1;
			foreach ($pengiriman as $o) {
				?>

				<?php if ($peritem >= $mulai_data && $peritem <= $mulai_data+5) : ?>
				
					<tr>
						<td class="nota2" style="text-align:left"><?= $o->log_no_order ?></td>
						<td class="nota2" style="text-align:left"><?= $o->log_po ?></td>
						<td class="nota2" style="text-align:left"><?= $o->log_pr ?></td>
						<td class="nota2" style="text-align:left"><?= $o->log_order ?></td>
						<td class="nota2" style="text-align:left"><?= $o->log_jumlah ?></td>
						<td class="nota2" style="text-align:left"><?= formatRupiah($o->log_harga_satuan) ?></td>
						<td class="nota2" style="text-align:left"><?= formatRupiah($o->log_total) ?></td>
						<td class="nota2" style="text-align:left"></td>
					</tr>

				<?php endif; ?>
				<?php
				$peritem++;
			}
			?>
			</tbody>
		</table>
	</div>
	<div>
		<table cellspacing="6" cellpadding="6">
		<tr>
				<td>Penerima, </td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami, </td>
			</tr>

			<tr>
				<td>Tanggal, <br><br><br><br></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengirim, <br><br><br><br></td>
			</tr>

		</table>
	</div>
</div>

<?php
if ($no > 0) :

?>
<div style="display: block; page-break-before: always; ">
	<!-- your new page content -->
	</div>
<?php 
endif;

?>

		<?php } ?>


</body>

</html>


