<div class="card-header">
  <div class="row">
    <a href="<?= site_url("Report/pegawai") ?>"> 
      <i class="btn btn-success fa fa-download"> Export Excel Pegawai</i>
    </a>
  </div>
  <?php echo form_open('laporan2');?>
    <div class="row my-3">
      <div class="form-group">
        <label for="tgl_awal">Dari Tanggal</label>
        <input type="date" id="tgl_awal" name="tgl_awal" value="<?= date("d-m-Y"); ?>" class="form-control">
      </div>
      <div class="form-group" style="padding-left:10px">
        <label for="tgl_akhir">Sampai Tanggal</label>
        <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control" value="<?= date('d-m-y') ?>" placeholder="Sampai Tanggal">
      </div>
    </div>
    <div class="row">
      <div class="form-group">  
        <button type="submit" id="filter_pertanggal" value="cari_taggal" name="cari_tanggal" class="btn btn-primary btn-flat btn-sm">
          <i class="fa fa-search"> Filter</i>
        </button>
      </div>
  </div>
<?php echo form_close();?>
<div class="table-responsive">
  <table class="table table-bodered table-striped" id="example1">
    <thead>
      <tr>
        <th>KD Kuskategori</th>
        <th>Nama Toko</th>
        <th>Kode Besoft</th>
        <th>Alamat</th>
        <th>Kota</th>
        <th>Telpon</th>
        <th>No NPWP</th>
      </tr>
    </thead>
    <tbody>
      <?php $no = 1;
        // if ($customer->num_rows() > 0 ) {
          foreach ($customer as $key => $value) 
          // print_r($customer);exit();
            { ?>

      <tr>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
            <td><?=$value->nama_toko ?></td>
        
      </tr>
      <?php }
      // }
      // else{
      //   echo 
      //     '<tr>
      //       <td colspan="8" class="text-center">Belum ada data pembelian</td> 
      //     </tr>';
      // }
      ?>
    </tbody>
  </table>
</div>

<script>
  $(document).ready(function(){
    var now = new Date();
    var month = (now.getMonth() + 1);
    var day = now.getDate();
    if (month < 10)
      month = "0" + month;
    if (day < 10)
      day = "0" + day;
    var today = now.getFullYear() + '-' + month + '-' + day;
    $('#tgl_awal').val(today);
    $('#tgl_akhir').val(today);
  });
</script>

   <!-- model -->
    private function _daftarrajal_datatables_query()
    {
        $column_search = ['kunjung.no_rm', 'master_dokter.dokter_nama', 'master_poli.poli_nama'];
        $this->db->select(['kunjung.kunjung_id', 'kunjung.no_rm', 'kunjung.antrian', 'kunjung.sts_inap', 'kunjung.sts_triage', 'kunjung.sts_skrining', 'master_poli.poli_nama', 'master_dokter.dokter_nama']);
        $this->db->from($this->_nama_tabel);
        $this->db->join('master_dokter', 'master_dokter.dokter_kd = kunjung.dokter_kd');
        $this->db->join('master_poli', 'master_poli.poli_kd = kunjung.poli_kd');
        $this->db->where(['kunjung.deleted' => 0, 'kunjung.tgl_masuk' => $this->input->post('cari_kunjung_tgl')]);
        $i = 0;
        foreach ($column_search as $item) {
            if ($this->input->post('cari_kunjung')) {
                if ($i === 0) {
                    $this->db->group_start();
                    $this->db->like($item, $this->input->post('cari_kunjung'));
                } else {
                    $this->db->or_like($item, $this->input->post('cari_kunjung'));
                }
                if (count($column_search) - 1 == $i) {
                    $this->db->group_end();
                }
            }
            $i++;
        }
        $this->db->order_by('kunjung.date_created', 'DESC');
    }

<!-- view  -->
<div class="col-sm-12 col-md-5 p0">
    <input autocomplete="off" type="text" name="cari_kunjung_tgl" id="cari_kunjung_tgl" class="form-control text-center" placeholder="Tanggal">
</div>

js                                <!--  -->
let tabel_kunjung = $('#tabel_kunjung').dataTable({
    "processing": true,
    "serverSide": true,

    "ajax": {
        "url": `${baseUrl}/rsibyl/pendaftaran/daftarrajal/datatables_kunjung`,
        "type": "POST",
        "data": function (data) {
            data.cari_kunjung = $('#cari_kunjung').val();
            data.cari_kunjung_tgl = moment($('#cari_kunjung_tgl').val(), 'DD-MM-YYYY').format("YYYY-MM-DD");
        }
    },
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bInfo": true,
    "bAutoWidth": true,
    "ordering": false,
    "pageLength": 10,
    "pagingType": "simple",
    "columnDefs": [
        {
            "targets": [0, 5, 6, 7],
            "visible": false,
        },
    ],

    <!-- kontroller -->
        public function datatables_kunjung()
    {
        $this->load->model(['M_kunjung']);
        $pasien = $this->M_kunjung->daftarrajal_get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($pasien as $r) {
            $no++;

            $row = array();
            $row[] = $r->kunjung_id;
            $row[] = $r->no_rm;
            $row[] = $r->antrian;
            $row[] = $r->poli_nama;
            $row[] = $r->dokter_nama;
            $row[] = $r->sts_inap;
            $row[] = $r->sts_triage;
            $row[] = $r->sts_skrining;

            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->M_kunjung->daftarrajal_count_all(),
            "recordsFiltered" => $this->M_kunjung->daftarrajal_count_filtered(),
            "data" => $data,
        );
        echo json_encode($output);
    }
