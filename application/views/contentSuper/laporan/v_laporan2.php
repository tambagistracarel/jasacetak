		<div class="content-wrapper">
    	<div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div>
        <div class="card card-info">
            <div class="card-header">
                <h1>Data Laporan</h1>
            </div>
            <div class="card-footer">
              <div class="col-lg-6">
                <div class="form-group">
                  <label for="">Pilih Data Laporan</label><br>
                  <select required  id="cek_jns_laporan" name="cek_jns_laporan" class="form-control">
                    <option value="" selected disabled>Pilih Data Laporan</option>
                    <option id="dt_customer" value="laporan_customer">Laporan Customer</option>
                    <option id="dt_pegawai" value="laporan_pegawai">Laporan Pegawai</option>
                    <option id="dt_pemesanan" value="laporan_per_pemesan">Laporan Per Pemesan</option>
                    <option id="dt_order_pertanggal" value="laporan_order_pertanggal">Laporan Order Per Tanggal</option>
                    
                  </select>
                </div>
              </div>
            </div>
        </section>

            <!--  -->
            <div class="bungkus-kostumer d-none" style="padding:1em">
                <div class="card-header">
                    <a href="<?= site_url("Report/customer") ?>"> 
                        <i class="btn btn-success fa fa-download"> Export Excel Customer</i>
                    </a>           
                </div>
                <div class="table-responsive">
                    <table id="data_kostumer" class="table table-bordered ">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Id Kostumer</th>
                            <th>Nama toko</th>
                            <th>alamat_toko</th>
                            <th>kota_toko</th>
                            <th>nama_owner</th>
                            <th>telepon owner</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!--  -->

            <!--  -->
            <div class="bungkus-pegawai d-none" style="padding:1em">
                <div class="card-header">
                    <a href="<?= site_url("Report/pegawai") ?>"> 
                        <i class="btn btn-success fa fa-download"> Export Excel Pegawai</i>
                    </a>           
                </div>
                <div class="table-responsive">
                    <table id="data_pegawai" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                            <th>No</th>
                            <th>Nama Pegawai</th>
                            <th>Jabatan</th>
                            <th>Kode Cabang</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!--  -->

            <!--  -->
            <div class="bungkus-per-pemesan d-none" style="padding:1em">
                <div class="row">
                    <div class="col-lg-12">
                        <table width="50%">
                            <tr>
                                <td> 
                                    <label for="pemesan"> Pilih Pemesan </label>
                                </td>
                                <td>
                                    <div class="form-group input-group">
                                        <!-- <input type="text" id="nama_toko"> -->
                                        <input type="" id="nama_toko" value="" class="form-control ">
                                            <span class="input-group-btn"> 
                                                <!-- Button trigger modal -->
                                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-pemesan">
                                                    <i class="fa fa-search"></i>
                                                    </button>
                                            </span>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="row">
                <div class="table-responsive">
                    <table id="data_per_pemesan" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No Order</th>
                                <th>Nama Toko</th>
                                <th>NO PO</th>
                                <th>NO PR</th>
                                <th>Tgl Order</th>
                                <th>Jenis Order</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                </div>
            </div>
            <!--  -->

            <!--  -->
            <div class="bungkus-order-pertanggal d-none" style="padding:1em">
                <div class="card-header">
                    <div class="row">
                      <a href="<?= site_url("Report/pegawai") ?>"> 
                        <i class="btn btn-success fa fa-download"> Export Excel Order Pertanggal</i>
                      </a>
                    </div>
                    <div class="row my-3">
                        <div class="form-group">
                          <label for="tgl_awal">Dari Tanggal</label>
                          <input type="date" id="tgl_awal" name="tgl_awal" value="<?= date("d-m-Y"); ?>" class="form-control">
                        </div>
                        <div class="form-group" style="padding-left:10px">
                          <label for="tgl_akhir">Sampai Tanggal</label>
                          <input type="date" name="tgl_akhir" id="tgl_akhir" class="form-control" value="<?= date('d-m-y') ?>" placeholder="Sampai Tanggal">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group">  
                          <button type="submit" id="cari_tanggal" value="cari_taggal" name="cari_tanggal" class="btn btn-primary btn-flat btn-sm">
                            <i class="fa fa-search"> Filter</i>
                          </button>
                        </div>
                    </div>
                <div class="table-responsive">
                    <table id="data_order_pertanggal" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Pemesan</th>
                                <th>Alamat</th>
                                <th>No Order</th>
                                <th>No PO</th>
                                <th>No PR</th>
                                <th>Tgl Order</th>
                                <th>Jenis Order</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <!--  -->

            <!--  -->
            <div class="bungkus-pemesanan d-none" style="padding:1em">
                <table id="data_pemesanan" class="table table-bordered">
                    <thead>
                        <tr>
                        <th>No</th>
                        <th>kode pesan</th>
                        <th>nama pesan</th>
                        <th>alamat</th>
                        <th>kota</th>
                        <th>telp</th>
                        <th>fak</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <!--  -->
    </div> 
    <!-- end tutup div html -->

<!-- modal perpemesan -->
<div class="modal fade" id="modal-pemesan">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4>Pilih Per Pemesan</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="close">
                    <span aria-hodden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body table-responsive">
                <table class="table table-bodered table-striped" id="example1" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>alamt</th>
                            <!-- <td>jenis</td> -->
                            <th>pilih</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($order as $o) {
                        ?>
                        <tr>
                                <td><?= $no++ ?></td>
                                <td><?= $o->nama_toko ?></td>
                                <td><?= $o->alamat_toko ?></td>
                                <!-- <td style="text-align:center"></td> -->
                                <td>
                                <button class="btn btn-xs btn-info" id="select"
                                 data-nama_toko="<?=$o->nama_toko?>"
                                >
                                    <i class="fa fa-check"></i>Select
                                </button>
                            </td>
                        </tr>
                        <?php
                        }
                        ?>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){
        resetTabel()
		$('#cek_jns_laporan').change(()=>{
            let jns = $('#cek_jns_laporan').val()
            if(jns == "laporan_pemesan"){
                resetTabel()
                $('#data_pemesanan').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "pageLength": 10,
                    "ajax": {
                            "url": "<?php echo site_url('laporan/ambil_pemesan') ?>",
                            "type": "POST",
                        },
                    });
                $('.bungkus-pemesanan').removeClass('d-none')
            }
            else if(jns == "laporan_per_pemesan"){
                resetTabel()
                $(document).on('click', '#select', function(){
                     $('#nama_toko').val($(this).data('nama_toko'))

                     var tangkap = $(this).data('nama_toko')
                     // alert(tangkap)

                     $('#modal-pemesan').modal('hide')
                    // var nama_toko = val($(this).data('nama_toko'))
                    // alert(nama_toko);

                    $('#data_per_pemesan').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "pageLength": 10,
                    "ajax": {
                            "url": "<?php echo site_url('laporan/ambil_order') ?>",
                            "type": "POST",
                            "data": function (data){

                            }
                        },
                    });
                })
                
                $('.bungkus-per-pemesan').removeClass('d-none')
            } 
            else if(jns == "laporan_pegawai"){
                resetTabel()
                $('#data_pegawai').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "pageLength": 10,
                    "ajax": {
                            "url": "<?php echo site_url('laporan/ambil_pegawai') ?>",
                            "type": "POST",
                        },
                    });
                $('.bungkus-pegawai').removeClass('d-none')
            }
            else if(jns == "laporan_order_pertanggal"){
                resetTabel()
                $('#data_order_pertanggal').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "pageLength": 10,
                    "ajax": {
                            "url"   : "<?php echo site_url('laporan/ambil_order_pertanggal') ?>",
                            "type"  : "POST",
                            // "data"  : function (data) {
                            //     console.log(data) 
                                // data.cari_kunjung = $('#cari_kunjung').val();
                                // data.cari_kunjung_tgl = moment($('#cari_kunjung_tgl').val(),'DD-MM-YYYY').format("YYYY-MM-DD");
                            // }
                        },
                    });
                $('.bungkus-order-pertanggal').removeClass('d-none')
            }
            else if(jns == "laporan_customer"){
                resetTabel()
                $('#data_kostumer').DataTable({
                    "processing": true,
                    "serverSide": true,
                    "ordering": false,
                    "pageLength": 10,
                    "ajax": {
                            "url": "<?php echo site_url('laporan/ambil_kostumer') ?>",
                            "type": "POST",
                        },
                    });
                $('.bungkus-kostumer').removeClass('d-none')
            }//end else if

        })//end else if

        $(document).on('keyup', '#tgl_awal', function(){
            var tgl_awal  = $('#tgl_awal').val() 
            var tgl_akhir = $('#tgl_akhir').val()


            
        })

        function resetTabel() {
            $('.bungkus-kostumer').addClass('d-none')
            $('.bungkus-pegawai').addClass('d-none')
            $('.bungkus-pemesanan').addClass('d-none')
            $('.bungkus-order-pertanggal').addClass('d-none')
            $('.bungkus-per-pemesan').addClass('d-none')
        }

        //format date
        var now = new Date();
        var month = (now.getMonth() + 1);
        var day = now.getDate();
        if (month < 10)
          month = "0" + month;
        if (day < 10)
          day = "0" + day;
        var today = now.getFullYear() + '-' + month + '-' + day;
        $('#tgl_awal').val(today);
        $('#tgl_akhir').val(today);
        //format date

	})
</script>