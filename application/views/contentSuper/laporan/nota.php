<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Print Struck Harga</title>
	<style type="text/css">
		html {
			font-family: "Verdana, Arial";
		}

		.content {
			width: 100%;
			font-size: 13px;
			/* padding: 5px; */
			border: 1px solid black;
		}

		.title {
			text-align: center;
			font-size: 13px;
			padding-bottom: 8px;
			border-bottom: 1px solid;
		}

		.head {
			margin-top: 5px;
			margin-bottom: 10px;
			padding-bottom: 10px;
			border-bottom: 1px solid;
		}

		.padding, .nota1, .nota2 {
		  border: 1px solid black;
		}

		.padding {
		  width: 100%;
		  border-collapse: collapse;
		}

		.table {
			width: 100%;
			font-size: 10px;
			
		}

		.thanks {
			/* margin-top: 10px; */
			padding-top: 12px;
			padding-bottom: 8px;
			text-align: center;
			font-size: 10px;
			border-top: 1px solid;
		}

		@media print {
			@page {
				width: 90mm;
				margin: 0mm
			}
		}
	</style>
</head>

<body onload="window.print()">
<div class="content ">
	<div class="title">
    <b>Percetakan dan Penerbitan</b>
		<b>CV.Andi Offset</b>
		<br>
		JL. BEO 38 - 40 TELP (0274) 561881
	</div>
	<div class="title">
		<b>SURAT PENGANTAR</b>
	</div>
	<div>
		<table cellspacing="3" cellpadding="3">
			<tr>
				<td>Yogyakarta, 10 agus</td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No : 1</td>
			</tr>

			<tr>
				<td>Kepada Yth : ayam<br><br><br><br></td>
			</tr>
			<tr>
				<td>Dengan hormat,<br></td>
			</tr>
			<tr>
				<td>Mohon agar diterima barang-barang cetakan sebagai berikut :</td>
			</tr>
		</table>
	</div>
	<div>
		<table class="padding">
			<thead>
			<tr>
				<th class="nota1" style="text-align:left">NO ORDER</th>
				<th class="nota1" style="text-align:left">NO PO</th>
				<th class="nota1" style="text-align:left">NO PR</th>
				<th class="nota1" style="text-align:left">NAMA ORDER</th>
				<th  class="nota1" style="text-align:left">JUMLAH</th>
				<th class="nota1" style="text-align:left">HARGA SATUAN</th>
				<th class="nota1" style="text-align:left">TOTAL HARGA</th>
				<!-- <th style="text-align:center">CATATAN</th> -->
			</tr>
			</thead>
			<tbody>
				<tr>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
					<td class="nota2"style="text-align:left">ayam</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div>
		<table cellspacing="6" cellpadding="6">
		<tr>
				<td>Penerima, </td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami, </td>
			</tr>

			<tr>
				<td>Tanggal, <br><br></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengirim, <br><br></td>
			</tr>

		</table>
	</div>
</div>

</body>

</html>
