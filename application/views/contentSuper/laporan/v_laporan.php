<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div>
        <div class="card card-info">
            <div class="card-header">
                <h1>Data Laporan</h1>
            </div>
              <div class="card-footer">
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="">Pilih Data Laporan</label><br>
                    <select id="cek_jns_laporan" name="cek_jns_laporan" class="form-control">
                      <option value="" selected disabled>Pilih Data Laporan</option>
                      <option id="dt_customer" value="laporan_customer">Laporan Customer</option>
                      <option id="dt_order_pertanggal" value="laporan_order_pertanggal">Laporan Order Pertanggal</option>
                      <option id="dt_order_pertanggal" value="laporan_per_pemesan">Laporan Per Pemesan</option>
                      <option id="" value="laporan_pengiriman_per_no_order">Laporan Pengirman Per No.Order</option>
                      <option id="" value="laporan_pengiriman_per_pemesan">Laporan Pengirman Per Pemesan</option>
                      <option id="" value="laporan_pengiriman_per_tanggal">Laporan Pengirman Per Tanggal</option>
                      <option id="" value="laporan_pembayaran_per_pemesan">Laporan Pembayaran Per Pemesan</option> 
                      <!-- <option id="" value="laporan_pembayaran_per_tanggal">Laporan Pembayaran Per Tanggal</option> -->
                      </select>
                  </div>
                </div>
              </div>
        </section>

        <section>
          <?php if ($this->uri->segment(3) == 'laporan_customer') { ?>
            <div class="card-header">
              <div class="row">
                <a href="<?= site_url('report/customer') ?>" >
                  <i class="btn btn-success fas fa-download"> Export Laporan Customer</i>
                </a>
              </div>
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                   <th>Nama</th>
                    <th>Kode Customer Besoft</th>
                    <th>Alamat</th>
                    <th>Kota</th>
                    <th>Telpon</th>
                    <th>No NPWP</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($customer as $cs) { ?>
                    <tr>
                      <td><?= $cs->nama_toko ?></td>
                      <td><?= $cs->kode_kustomer_besoft ?></td>
                      <td><?= $cs->alamat_toko ?></td>
                      <td><?= $cs->kota_toko ?></td>
                      <td><?= $cs->telepon_toko ?></td>
                      <td><?= $cs->no_npwp ?></td>
                    </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_order_pertanggal') { ?>
            <div class="card-header">
              <div class="row">
                <a id="excel_order_pertanggal">
                  <i class="btn btn-success fas fa-download"> Export Laporan Order Pertanggal</i>
                </a>
              </div>
              <div class="row my-3">
                <div class="form-group">
                  <label for="dates">Pilih Tanggal </label>
                    <input type="text" value="<?=date('d-m-Y')?>" class="form-control" name="dates" id="dates" placeholder="Pilih Tanggal" />
                  </div>
                </div>
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Pemesan</th>
                    <th>Alamat</th>
                    <th>No Order</th>
                    <th>No PO</th>
                    <th>No PR</th>
                    <th>Tgl Order</th>
                    <th>Jenis Order</th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach ($order_pertanggal as $cs) { ?>
                    <tr>
                      <td><?= $cs->nama_toko ?></td>
                      <td><?= $cs->alamat_toko ?></td>
                      <td><?= $cs->no_order ?></td>
                      <td><?= $cs->no_po ?></td>
                      <td><?= $cs->no_pr ?></td>
                      <td><?= $cs->no_pr ?></td>
                      <td><?= $cs->tgl_order ?></td>
                      <td><?= $cs->jns_order ?></td>
                    </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_pengiriman_per_tanggal') { ?>
            <div class="card-header">
              <div class="row">
                <a id="excel_pengiriman_pertanggal">
                  <i class="btn btn-success fas fa-download"> Export Laporan Pengiriman Per Tanggal</i>
                </a>
              </div>
              <div class="row my-3">
                <div class="form-group">
                  <label for="dates">Pilih Tanggal </label>
                    <input type="text" value="<?=date('d-m-Y')?>" class="form-control" name="dates" id="dates" placeholder="Pilih Tanggal" />
                  </div>
                </div>
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>tanggal Pengiriman</th>
                    <th>Nama Pemesan</th>
                    <th>Alamat</th>
                    <th>No Order</th>
                    <th>No PO</th>
                    <th>No PR</th>
                    <th>Jenis Order</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($pengiriman_pertanggal as $cs) { ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $cs->tgl_minta_kirim ?></td>
                      <td><?= $cs->nama_toko ?></td>
                      <td><?= $cs->alamat_toko ?></td>
                      <td><?= $cs->no_order ?></td>
                      <td><?= $cs->no_po ?></td>
                      <td><?= $cs->no_pr ?></td>
                      <td><?= $cs->jns_order ?></td>
                    </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_per_pemesan') { ?>
            <div class="card-header">
              <div class="row">
                  <div class="col-lg-12">
                    <table width="50%">
                      <tr>
                        <td> 
                          <label for="pemesan"> Pilih Pemesan </label>
                        </td>
                        <td>
                          <div class="form-group input-group">
                            <!-- <input type="text" id="nama_toko"> -->
                            <input type="" id="nama_toko" name="nama_toko" class="form-control" readonly>
                              <span class="input-group-btn"> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-per-pemesan">
                                <i class="fa fa-search"></i>
                                </button>
                              </span>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="row bungkus-detail-per-pemesan d-none">
                <div class="card-header">
                  <a id="export_detail_per_pemesan"> 
                    <i class="btn btn-success fas fa-download"> Export Excel Per Pemesan</i>
                  </a>           
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped" width="100%" id="data_per_pemesan">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Order</th>
                        <th>Nama Toko</th>
                        <th>Alamat</th>
                        <th>NO PO</th>
                        <th>NO PR</th>
                        <th>Tgl Order</th>
                        <th>Tgl Kirim</th>
                        <th>Jenis Order</th>
                      </tr>
                    </thead>
                    <tbody id="data_detail_perpemesan">
                      <!-- isi dengan ajax -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_pengiriman_per_no_order') { ?>
            <div class="card-header">
              <div class="row">
                  <div class="col-lg-12">
                    <table width="50%">
                      <tr>
                        <td> 
                          <label for="pemesan"> Pilih No Order </label>
                        </td>
                        <td>
                          <div class="form-group input-group">
                            <!-- <input type="text" id="nama_toko"> -->
                            <input type="" id="pengiriman_no_order" name="pengiriman_no_order" class="form-control" readonly>
                              <span class="input-group-btn"> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-pengiriman-no-order">
                                <i class="fa fa-search"></i>
                                </button>
                              </span>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="row bungkus-pengiriman-per-no-order d-none">
                <div class="card-header">
                  <a id="export_detail_pengiriman_per_no_order"> 
                    <i class="btn btn-success fas fa-download"> Export Excel Pengiriman Per No.Order</i>
                  </a>           
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped" id="data_pengiriman_no_order" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Order</th>
                        <th>Tanggal Kirim</th>
                        <th>Nama Pemesan</th>
                        <th>No PO</th>
                        <th>No PR</th>
                        <th>Jenis Order</th>
                        <th>Jumlah</th>
                      </tr>
                    </thead>
                    <tbody id="data_pengiriman_per_no_order">
                      <!-- isi dengan ajax -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_pengiriman_per_pemesan') { ?>
            <div class="card-header">
              <div class="row">
                  <div class="col-lg-12">
                    <table width="50%">
                      <tr>
                        <td> 
                          <label for="pemesan"> Pilih Pemesan </label>
                        </td>
                        <td>
                          <div class="form-group input-group">
                            <!-- <input type="text" id="nama_toko"> -->
                            <input type="" id="pengiriman_per_pemesan" name="pengiriman_per_pemesan" class="form-control" readonly>
                              <span class="input-group-btn"> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-pengiriman-per-pemesan">
                                <i class="fa fa-search"></i>
                                </button>
                              </span>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="row bungkus-pengiriman-per-pemesan d-none">
                <div class="card-header">
                  <a id="export_detail_pengiriman_per_pemesan"> 
                    <i class="btn btn-success fas fa-download"> Export Excel Pengiriman Per Pemesan</i>
                  </a>           
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped" id="data_export_pengiriman_per_pemesan" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Tanggal Kirim</th>
                        <th>No Order</th>
                        <th>Nama Pemesan</th>
                        <th>Jenis Pemesan</th>
                        <th>No PO</th>
                        <th>No PR</th>
                        <th>Jumlah</th>
                        <th>Pengirim</th>
                      </tr>
                    </thead>
                    <tbody id="data_pngiriman_per_pemesan">
                      <!-- isi dengan ajax -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>

          <?php if ($this->uri->segment(3) == 'laporan_pembayaran_per_pemesan') { ?>
            <div class="card-header">
              <div class="row">
                  <div class="col-lg-12">
                    <table width="50%">
                      <tr>
                        <td> 
                          <label for="pemesan"> Pilih Pemesan </label>
                        </td>
                        <td>
                          <div class="form-group input-group">
                            <!-- <input type="text" id="nama_toko"> -->
                            <input type="" id="pembayaran_per_pemesan" name="pembayaran_per_pemesan" class="form-control" readonly>
                              <span class="input-group-btn"> 
                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-pembayaran-per-pemesan">
                                <i class="fa fa-search"></i>
                                </button>
                              </span>
                          </div>
                        </td>
                      </tr>
                    </table>
                  </div>
              </div>
              <div class="row bungkus-pembayaran-per-pemesan d-none">
                <div class="card-header">
                  <a id="export_detail_pembayaran_per_pemesan"> 
                    <i class="btn btn-success fas fa-download"> Export Excel Pembayaran Per Pemesan</i>
                  </a>           
                </div>
                <div class="table-responsive">
                  <table class="table table-bordered table-striped" id="data_pembayaran_per_pemesan" width="100%">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>No Order</th>
                        <th>Nama Customer</th>
                        <th>Jenis Bayar</th>
                        <th>Harga Satuan</th>
                        <th>Total Harga</th>
                        <th>Cicilan</th>
                        <th>Total Bayar</th>
                        <th>Jenis Pembayaran</th>
                      </tr>
                    </thead>
                    <tbody id="data_pembayaran_per_pemesan">
                      <!-- isi dengan ajax -->
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          <?php } ?>

          <!-- <?php if ($this->uri->segment(3) == 'laporan_pembayaran_per_tanggal') { ?>
            <div class="card-header">
              <div class="row">
                <a id="excel_pembayaran_pertanggal">
                  <i class="btn btn-success fas fa-download"> Export Laporan pembayaran Per Tanggal</i>
                </a>
              </div>
              <div class="row my-3">
                <div class="form-group">
                  <label for="dates">Pilih Tanggal </label>
                    <input type="text" value="<?=date('d-m-Y')?>" class="form-control" name="dates" id="dates" placeholder="Pilih Tanggal" />
                  </div>
                </div>
            <div class="table-responsive">
              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>No Order</th>
                    <th>Nama Customer</th>
                    <th>Jenis Bayar</th>
                    <th>Jumlah</th>
                    <th>Harga Satuan</th>
                    <th>Total Harga</th>
                    <th>Cicilan</th>
                    <th>Total Bayar</th>
                    <th>Jenis Pembayaran</th>
                  </tr>
                </thead>
                <tbody>
                  <?php $no=1; foreach ($pembayaran_pertanggal as $cs) { ?>
                    <tr>
                      <td><?= $no++ ?></td>
                      <td><?= $cs->log_no_order ?></td>
                      <td><?= $cs->log_customer ?></td>
                      <td><?= $cs->log_order ?></td>
                      <td><?= $cs->log_jumlah ?></td>
                      <td><?= $cs->log_harga_satuan ?></td>
                      <td><?= $cs->log_total ?></td>
                      <td><?= $cs->log_cicilan ?></td>
                      <td><?= $cs->log_pembayaran ?></td>
                      <td><?= $cs->log_jns_bayar ?></td>
                    </tr>
                <?php }?>
                </tbody>
              </table>
            </div>
          <?php } ?> -->
        </section>
    </div>

<!-- modal perpemesan -->
<div class="modal fade" id="modal-per-pemesan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Pilih Per Pemesan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="close">
            <span aria-hodden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body table-responsive">
          <table class="table table-bodered table-striped example1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>alamt</th>
                    <!-- <td>jenis</td> -->
                    <th>pilih</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($per_pemesan as $o) {
                ?>
                <tr>
                        <td><?= $no++ ?></td>
                        <td><?= $o->nama_toko ?></td>
                        <td><?= $o->alamat_toko ?></td>
                        <!-- <td style="text-align:center"></td> -->
                        <td>
                        <button class="btn btn-xs btn-info" id="per-pemesan"
                         data-nama_toko="<?=$o->nama_toko?>"
                        >
                            <i class="fa fa-check"></i>Select
                        </button>
                    </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
          </table>  
        </div>
      </div>
  </div>
</div>

<!-- modal pengiriman no order -->
<div class="modal fade" id="modal-pengiriman-no-order">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Pilih No Order</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="close">
            <span aria-hodden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body table-responsive">
          <table class="table table-bodered table-striped example1" >
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Order</th>
                    <!-- <td>jenis</td> -->
                    <th>pilih</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($pengiriman_no_order as $pno) {
                ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $pno->no_order ?></td>
                  <!-- <td style="text-align:center"></td> -->
                  <td>
                    <button class="btn btn-xs btn-info" id="select_modal_pengiriman_no_order"
                      data-no_order="<?=$pno->no_order?>"
                    >
                      <i class="fa fa-check"></i>Select
                    </button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
          </table>  
        </div>
      </div>
  </div>
</div>

<!-- modal pengiriman per pemesan -->
<div class="modal fade" id="modal-pengiriman-per-pemesan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Pilih Per Pemesan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="close">
            <span aria-hodden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body table-responsive">
          <table class="table table-bodered table-striped example1" >
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Alamat</th>
                    <!-- <td>jenis</td> -->
                    <th>pilih</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($pengiriman_per_pemesan as $ppp) {
                ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $ppp->nama_toko ?></td>
                  <td><?= $ppp->alamat_toko ?></td>
                  <!-- <td style="text-align:center"></td> -->
                  <td>
                    <button class="btn btn-xs btn-info" id="select_modal_pengiriman_per_pemesan"
                      data-per_pemesan="<?=$ppp->nama_toko?>"
                    >
                      <i class="fa fa-check"></i>Select
                    </button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
          </table>  
        </div>
      </div>
  </div>
</div>

<!-- modal pembayaran per pemesan -->
<div class="modal fade" id="modal-pembayaran-per-pemesan">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4>Pilih Data Pemesan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="close">
            <span aria-hodden="true">&times;</span>
        </button>
      </div>
        <div class="modal-body table-responsive">
          <table class="table table-bodered table-striped example1" >
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Order</th>
                    <th>Nama Customer</th>
                    <!-- <td>jenis</td> -->
                    <th>pilih</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($pembayaran_per_pemesan as $ppp) {
                ?>
                <tr>
                  <td><?= $no++ ?></td>
                  <td><?= $ppp->log_no_order ?></td>
                  <td><?= $ppp->log_customer ?></td>
                  <!-- <td style="text-align:center"></td> -->
                  <td>
                    <button class="btn btn-xs btn-info" id="select_modal_pembayaran_per_pemesan"
                      data-nama_toko="<?=$ppp->log_customer?>"
                    >
                      <i class="fa fa-check"></i>Select
                    </button>
                  </td>
                </tr>
                <?php
                }
                ?>
              </tbody>
          </table>  
        </div>
      </div>
  </div>
</div>


<script>
  // $('#example1').dataTable().fnDestroy();
  var value_laporan
    $("#cek_jns_laporan").change(function() { 
      value_laporan = $(this).val();
      // alert(value);
      window.location.href = '<?= base_url() ?>laporan/pilih_laporan/'+value_laporan;
    })

    //logika filter tanggal
    $(function() {
      let urlTerpilih = '<?= $this->uri->segment(3) ?>';
      // console.log(urlTerpilih);
      $('input[name="dates"]').daterangepicker({
        opens: 'left'
      }, function(start, end, label) {
          $("#dates").change(function() {
            var hasil_tanggal = start.format('YYYY-MM-DD')+'_'+end.format('YYYY-MM-DD')        
            // console.log(start.format('DD-MM-YYYY'))
            // console.log(end.format('DD-MM-YYYY'))
            // console.log(hasil_tanggal)
            if(urlTerpilih == 'laporan_order_pertanggal') {
              window.location.href = '<?= base_url() ?>laporan/pilih_laporan/'+urlTerpilih+'/'+hasil_tanggal;
            }
            else if (urlTerpilih == 'laporan_pengiriman_per_tanggal'){
              window.location.href = '<?= base_url() ?>laporan/pilih_laporan/'+urlTerpilih+'/'+hasil_tanggal;
            }
          })
      });
    })

    //filter data order pertanggal excel
    $(document).on('click', '#excel_order_pertanggal', function(){
      let urlTerpilih = '<?= $this->uri->segment(3) ?>';
      let TanggalTerpilih = '<?= $this->uri->segment(4) ?>';
       window.location.href = '<?= base_url() ?>report/laporan_order_pertanggal/'+TanggalTerpilih;
    })

    $(document).on('click', '#excel_pengiriman_pertanggal', function(){
      let urlTerpilih = '<?= $this->uri->segment(3) ?>';
      let TanggalTerpilih = '<?= $this->uri->segment(4) ?>';
       window.location.href = '<?= base_url() ?>report/laporan_pengiriman_pertanggal/'+TanggalTerpilih;
    })

    $(document).on('click', '#select_modal_pengiriman_no_order', function(){
      // alert()
      $('#pengiriman_no_order').val($(this).data('no_order'))
      var tangkap_no_order = $(this).data('no_order')
      $('#modal-pengiriman-no-order').modal('hide')
      if (tangkap_no_order == ''){
        alert('Mohon pilih No Order')
      }else{
        $.ajax({
          type    : 'POST',
          url     : '<?=site_url('laporan/detail_pengiriman_per_no_order') ?>',//menangkap data yang diparsing dari controller.
          // async   : true,
          data   : { detail_pengiriman_per_no_order : true, 'tangkap_pengiriman_no_order' : tangkap_no_order },
          dataType: 'json',
          success : function(data){
            var html = '';
            var i;
            var no = 1;
            if(data.length >0){
              for(i=0; i<data.length; i++){
                html += 
                  '<tr>'+
                    '<td>'+ no++ +'</td>'+
                    '<td>'+data[i].no_order+'</td>'+
                    '<td>'+data[i].tgl_minta_kirim+'</td>'+
                    '<td>'+data[i].nama_toko+'</td>'+
                    '<td>'+data[i].alamat_toko+'</td>'+
                    '<td>'+data[i].no_po+'</td>'+
                    '<td>'+data[i].no_pr+'</td>'+
                    '<td>'+data[i].set_buku+'</td>'+
                  '</tr>';
              }
            }
            else{
              html += `<tr>
                          <td colspan=4 > Tidak ada data </td>
                      </tr>`
            }
            $('#data_pengiriman_per_no_order').html(html);
            // $('#example1').dataTable().fnDestroy();
          }
        })
        $('.bungkus-pengiriman-per-no-order').removeClass('d-none')
        // $('.example1').removeClass('example1')
      }
    })

    $(document).on('click', '#select_modal_pengiriman_per_pemesan', function(){
      // alert()
      $('#pengiriman_per_pemesan').val($(this).data('per_pemesan'))
      var tangkap_pengiriman_per_pemesan = $(this).data('per_pemesan')
      $('#modal-pengiriman-per-pemesan').modal('hide')
      if (tangkap_pengiriman_per_pemesan == ''){
        alert('Mohon pilih No Order')
      }else{
        $.ajax({
          type    : 'POST',
          url     : '<?=site_url('laporan/detail_pengiriman_per_pemesan') ?>',//menangkap data yang diparsing dari controller.
          // async   : true,
          data   : { detail_pengiriman_per_pemesan : true, 'tangkap_pengiriman_per_pemesan' : tangkap_pengiriman_per_pemesan },
          dataType: 'json',
          success : function(data){
            var html = '';
            var i;
            var no = 1;
            if(data.length >0){
              for(i=0; i<data.length; i++){
                html += 
                  '<tr>'+
                    '<td>'+ no++ +'</td>'+
                    '<td>'+data[i].no_order+'</td>'+
                    '<td>'+data[i].tgl_minta_kirim+'</td>'+
                    '<td>'+data[i].nama_toko+'</td>'+
                    '<td>'+data[i].alamat_toko+'</td>'+
                    '<td>'+data[i].no_po+'</td>'+
                    '<td>'+data[i].no_pr+'</td>'+
                    '<td>'+data[i].set_buku+'</td>'+
                  '</tr>';
              }
            }
            else{
              html += `<tr>
                          <td colspan=4 > Tidak ada data </td>
                      </tr>`
            }
            $('#data_pngiriman_per_pemesan').html(html);
            // $('#example1').dataTable().fnDestroy();
          }
        })
        $('.bungkus-pengiriman-per-pemesan').removeClass('d-none')
        // $('.example1').removeClass('example1')
      }
    })

    $(document).on('click', '#select_modal_pembayaran_per_pemesan', function(){
      // alert()
      $('#pembayaran_per_pemesan').val($(this).data('nama_toko'))
      var tangkap_pembayaran_per_pemesan = $(this).data('nama_toko')
      $('#modal-pembayaran-per-pemesan').modal('hide')
      if (tangkap_pembayaran_per_pemesan == ''){
        alert('Mohon pilih Nama Pemesan')
      }else{
        $.ajax({
          type    : 'POST',
          url     : '<?=site_url('laporan/detail_pembayaran_per_pemesan') ?>',//menangkap data yang diparsing dari controller.
          // async   : true,
          data   : { detail_pembayaran_per_pemesan : true, 'tangkap_pembayaran_per_pemesan' : tangkap_pembayaran_per_pemesan },
          dataType: 'json',
          success : function(data){
            var html = '';
            var i;
            var no = 1;
            if(data.length >0){
              for(i=0; i<data.length; i++){
                html += 
                  '<tr>'+
                    '<td>'+ no++ +'</td>'+
                    '<td>'+data[i].log_no_order+'</td>'+
                    '<td>'+data[i].log_customer+'</td>'+
                    '<td>'+data[i].log_order+'</td>'+
                    '<td>'+data[i].log_harga_satuan+'</td>'+
                    '<td>'+data[i].log_total+'</td>'+
                    '<td>'+data[i].log_cicilan+'</td>'+
                    '<td>'+data[i].log_pembayaran+'</td>'+
                    '<td>'+data[i].log_jns_bayar+'</td>'+
                  '</tr>';
              }
            }
            else{
              html += `<tr>
                          <td colspan=4 > Tidak ada data </td>
                      </tr>`
            }
            $('#data_pembayaran_per_pemesan').html(html);
            // $('#example1').dataTable().fnDestroy();
          }
        })
        $('.bungkus-pembayaran-per-pemesan').removeClass('d-none')
        // $('.example1').removeClass('example1')
      }
    })

    //menampil detail per pemesan
    $(document).on('click', '#per-pemesan', function(){
      // alert()
      $('#nama_toko').val($(this).data('nama_toko'))
      var tangkap_nama = $(this).data('nama_toko')
      $('#modal-per-pemesan').modal('hide')
      // $(document).on('click', '#export_detail_per_pemesan', function(){
      //   // alert(tangkap_nama)
      //   if (tangkap_nama == ''){
      //     alert('Data Tidak Ada')
      //   }else{
      //     $.ajax({
      //       type    : 'POST',
      //       url     : '<?=site_url('report/export_detail_per_pemesan') ?>',
      //       data   : { export_detail_per_pemesan : true, 'tangkap_nama' : tangkap_nama },
      //       dataType: 'json',
      //       success : function(data){
      //         console.log(data)
      //         // if (data == "berhasil"){
      //           // window.location.href = '<?= base_url() ?>report/export_detail_per_pemesan/';
      //         // }
      //       }
      //     })
      //   }
      // })
      if (tangkap_nama == ''){
        alert('Mohon pilih Data Pemesan')
      }else{
        $.ajax({
          type    : 'POST',
          url     : '<?=site_url('laporan/detail_per_pemesan') ?>',//menangkap data yang diparsing dari controller.
          // async   : true,
          data   : { detail_pemesan : true, 'tangkap_nama' : tangkap_nama },
          dataType: 'json',
          success : function(data){
            var html = '';
            var i;
            var no = 1;
            if(data.length >0){
              for(i=0; i<data.length; i++){
                html += 
                  '<tr>'+
                    '<td>'+ no++ +'</td>'+
                    '<td>'+data[i].no_order+'</td>'+
                    '<td>'+data[i].nama_toko+'</td>'+
                    '<td>'+data[i].alamat_toko+'</td>'+
                    '<td>'+data[i].no_po+'</td>'+
                    '<td>'+data[i].no_pr+'</td>'+
                    '<td>'+data[i].tgl_order+'</td>'+
                    '<td>'+data[i].tgl_minta_kirim+'</td>'+
                    '<td>'+data[i].jns_order+'</td>'+
                  '</tr>';
              }
            }
            else{
              html += `<tr>
                          <td colspan=4 > Tidak ada data </td>
                      </tr>`
            }
            $('#data_detail_perpemesan').html(html);
            // $('#example1').dataTable().fnDestroy();
          }
        })
        $('.bungkus-detail-per-pemesan').removeClass('d-none')
        // $('.example1').removeClass('example1')
      }
      // alert(tangkap) 
    })


//=======================================//
//========== button export excel ========//
//=======================================//
  $(document).on('click', '#export_detail_per_pemesan', function(){
    $("#data_per_pemesan").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet Name",
        filename: "Data Per Pemesan.xls", // do include extension
        preserveColors: false // set to true if you want background colors and font colors preserved
    });  
  })

  $(document).on('click', '#export_detail_pengiriman_per_no_order', function(){
    $("#data_pengiriman_no_order").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet Name",
        filename: "Data Pengiriman No Order.xls", // do include extension
        preserveColors: false // set to true if you want background colors and font colors preserved
    });  
  })

  $(document).on('click', '#export_detail_pengiriman_per_pemesan', function(){
    $("#data_export_pengiriman_per_pemesan").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet Name",
        filename: "Data Pengiriman Per Pemesan.xls", // do include extension
        preserveColors: false // set to true if you want background colors and font colors preserved
    });  
  })

  $(document).on('click', '#export_detail_pembayaran_per_pemesan', function(){
    $("#data_pembayaran_per_pemesan").table2excel({
        exclude: ".excludeThisClass",
        name: "Worksheet Name",
        filename: "Data Pembayaran Per Pemesan.xls", // do include extension
        preserveColors: false // set to true if you want background colors and font colors preserved
    });  
  })
</script>