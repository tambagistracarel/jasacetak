<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>

            </div>
        </div>
    </div>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-info">
                    <div class="card-header">
                        <h1>Ubah Data Pegawai</h1>
                    </div>
                    <div class="card-body">
                        <form id="form-ubah-pegawai" method="post" action="<?= site_url('Pegawai/proses_update') ?>" role="form">
                            <div class="row">
                                <div class="col-sm-12">
                                <div class="form-group">
                                        <label>id Anggota</label>
                                        <input readonly type="text" class="form-control form-control-sm" id="id_anggota" name="id_anggota" value="<?= $pegawais->id_anggota ?>" placeholder="Enter ..." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Nama Anggota</label>
                                        <input type="text" class="form-control form-control-sm" id="nama_anggota" name="nama_anggota" value="<?= $pegawais->nama_anggota ?>" placeholder="Enter ..." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <input type="text" class="form-control form-control-sm" id="jabatan" name="jabatan" value="<?= $pegawais->jabatan ?>" placeholder="Enter ..." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <input type="text" class="form-control form-control-sm" id="alamat_anggota" name="alamat_anggota" value="<?= $pegawais->alamat_anggota ?>" placeholder="Enter ...">
                                    </div>
                                    <div class="form-group">
                                        <label>No Telepon</label>
                                        <input type="text" class="form-control form-control-sm" id="telepon_anggota" name="telepon_anggota" value="<?= $pegawais->telepon_anggota ?>" placeholder="Enter ...">
                                    </div>
                                    <div class="form-group">
                                        <label>Keterangan </label>
                                        <input type="text" class="form-control form-control-sm" id="keterangan_anggota" name="keterangan_anggota" value="<?= $pegawais->keterangan_anggota ?>" placeholder="Enter ..." required>
                                    </div>
                            </div>


                    </div>
                    <div class="card-footer">
                        <button id="btn-save" class="btn btn-sm btn-success"><i class="fas fa-edit"></i>Ubah</button>
                    </div>
                    <input type="hidden" id="id_anggota" name="id_anggota" value="<?= $pegawais->id_anggota; ?>" />
                    </form>
                </div>
            </div>
        </div>
</div>
</section>
<!--  -->
</div>
<script>
    $(function() {
        $("#btn-save").on("click", function() {
            let validate = $("#form-ubah-pegawai").valid();
            if (validate) {
                Swal.fire({
								icon: 'success',
								title: 'Selamat',
								text: ' Data pegawai telah du ubah',
							});
                $("#form-ubah-pegawai").submit();
            }
        });
        $("#form-ubah-pegawai").validate({
            rules: {
                nomer: {
                    digits: true
                },
                alamat: {
                    required: true
                }
            },
            messages: {
                kode: {
                    digits: "Hanya nomer saja"
                }
            },
            errorElement: 'span',
            errorPlacement: function(error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function(element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function(element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    });
</script>
