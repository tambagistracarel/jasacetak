<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">

            </div>
        </div>
        <div class="card card-info">
            <div class="card-header">
                <h1>Data Pegawai</h1>
            </div>
            <div class="card-footer">
                <a href="<?= site_url(array("Pegawai", "register")) ?>" class="btn btn-success "><i class="fas fa-folder-plus"></i>
                    Tambah Data Pegawai
                </a> &nbsp;
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="text-align:center"> No</th>
                            <th style="text-align:center"> Id Anggota</th>
                            <th style="text-align:center">Nama Anggota</th>
                            <th style="text-align:center">Jabatan </th>
                            <th style="text-align:center">Alamat</th>
                            <th style="text-align:center">No Telepon</th>
                            <th style="text-align:center">Keterangan</th>
                            <th style="text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($pegawais as $pegawai) {
                        ?>
                            <tr>
                                <td style="text-align:center"><?= $no++ ?></td>
                                <td style="text-align:center"><?= $pegawai->id_anggota ?></td>
                                <td style="text-align:center"><?= $pegawai->nama_anggota ?></td>
                                <td style="text-align:center"><?= $pegawai->jabatan ?></td>
                                <td style="text-align:center"><?= $pegawai->alamat_anggota ?></td>
                                <td style="text-align:center"><?= $pegawai->telepon_anggota ?></td>
                                <td style="text-align:center"><?= $pegawai->keterangan_anggota ?></td>
                                <td style="text-align:center">
                                    <a href="<?= site_url("Pegawai/update/$pegawai->id_anggota") ?>" 
                                        class="btn btn-sm btn-warning" data-title="Edit"><i class="fas fa-edit"></i></a>
                                </td>
                            </tr>
                        <?php
                        }
                        ?>
                    </tbody>    
                </table>
            </div>
        </div>
        </section>
    </div>
    <script>
         $(function() {
            let idAnggota = 0;
            $(".tombolHapus").on("click", function() {
                var id = $(this).data('id');
                SwalDelete(id);
            });
        });

        function SwalDelete(id) {
            Swal.fire({
                title: ' Hapus Data Pegawai Ini?',
                text: " ",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#20B2AA',
                cancelButtonColor: '#FF7F00',
                confirmButtonText: 'Hapus Data ',
                showLoaderOnConfirm: true,
                preConfirm: function() {
                    return new Promise(function(resolve) {
                        var url = "pegawai/proses_hapus/"
                        $.ajax({
                                url: '<?= base_url() ?>' + url + id,
                                type: "POST",
                            })
                            .done(function(id) {
                                window.location.replace("<?= site_url("Pegawai") ?>");
                                Swal.fire('Hapus Data Berhasil', 'Data Anda Telah Terhapus!', 'success')
                            })
                            .fail(function() {
                                Swal.fire('Maaf', 'Data Anda Sudah Masuk proses Transaksi', 'error')
                            });
                    });
                },
            });
        }
    </script>
