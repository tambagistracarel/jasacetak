<div class="content-wrapper">
	<section class="content">
		<form enctype="multipart/form-data" method="post" action="<?= site_url("Transaksi/proses_bayar"); ?>"
			  role="form">
			<div class="row">
				<div class="col-md-5">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<input type="hidden" id="no-order" value="<?= $order->no_order ?>"
									   name="no_order" class="form-control"/>
								<label for="">Harga</label>
								<input readonly type="text" min="1" id="tampung_harga" value="<?= $order->tampung_harga ?>"
									   name="tampung_harga" class="form-control"/>
							</div>
							<div class="form-group">
								<label for="">Pembayaran</label>
								<input required type="text" id="bayar" value="" name="bayar" class="form-control"/>
								<input type="hidden" id="tampung_bayar" value="<?= $order->tampung_bayar ?>" name="tampung_bayar" class="form-control"/>
								<input type="hidden" id="nama_toko" value="<?= $order->nama_toko ?>" name="nama_toko" class="form-control"/>
								<input type="hidden" id="no_po" value="<?= $order->no_po ?>" name="no_po" class="form-control"/>
								<input type="hidden" id="no_pr" value="<?= $order->no_pr ?>" name="no_pr" class="form-control"/>
								<input type="hidden" id="jns_order" value="<?= $order->jns_order ?>" name="jns_order" class="form-control"/>
								<input type="hidden" id="buku" value="<?= $order->buku ?>" name="buku" class="form-control"/>
								<input type="hidden" id="set_buku" value="<?= $order->set_buku ?>" name="set_buku" class="form-control"/>
								<input type="hidden" id="eks" value="<?= $order->eks ?>" name="eks" class="form-control"/>
								<input type="hidden" id="harga_order" value="<?= $order->harga_order ?>" name="harga_order" class="form-control"/>
								<input type="hidden" id="total_semua" value="<?= $order->total_semua ?>" name="total_semua" class="form-control"/>
							</div>
							<div class="form-group">
								<label for="">Total</label>
								<input readonly type="text" id="total" value="<?= $order->total ?>" name="total"
									   class="form-control"/>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7">
					<div class="card">
						<div class="card-header"><h5>Jenis Pembayaran</h5></div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<select required onchange="cek_jns_pembayaran(this)" name="jns_bayar"
												class="form-control"
												id="select-jenis-pembayaran">
											<option value="" selected disabled>Pilih Jenis Pembayaran</option>
											<option id="tunai" value="TUNAI">TUNAI</option>
											<option id="tf" value="TRANSFER">TRANSFER</option>
										</select>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group" id="select-bank" style="visibility: hidden">
										<select name="nama_bank" class="form-control" id="select-bank-bayar">
											<option value="" disabled selected>Pilih Bank</option>
											<?php
											foreach ($banks as $c) {
												echo "<option data-bank='$c->nama_bank' "
														. "data-rek='$c->no_rek' "
														. "data-nama='$c->atas_nama' "
														. "value='$c->id_bank'> "
														. "$c->nama_bank"
														. "</option>";
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<input readonly type="number" placeholder="NO REKENING" min="1" style="display: none"
											   class="form-control" name="no_rek" id="no-rek">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<input readonly type="text" placeholder="ATAS NAMA" style="display: none"
											   class="form-control" name="atas_nama" id="atas-nama">
									</div>
								</div>
								<input type="hidden" class="form-control" name="nomor" value="<?= $order->nomor ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="card-body">
					<input type="submit" id="btn-simpan" value="Simpan" class="btn btn-primary float-left">
				</div>
			</div>
		</form>
	</section>
</div>
<!--<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>-->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="jquery.masknumber.js"></script>
<script>
	function cek_bank(obj) {
		var value = obj.value;
		if (value == "BRI") {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('atas-nama').style.display = 'block';
		} else if (value == "BNI") {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('atas-nama').style.display = 'block';
		} else if (value == "MANDIRI") {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('atas-nama').style.display = 'block';
		} else if (value == "BCA") {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('atas-nama').style.display = 'block';
		}
	}

	function cek_jns_pembayaran(obj) {
		var value = obj.value;
		if (value == "TUNAI") {
			document.getElementById('no-rek').style.display = 'none';
			document.getElementById('select-bank').style.visibility = 'hidden';
			document.getElementById('atas-nama').style.display = 'none';
		} else {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('select-bank').style.visibility = 'visible';
			document.getElementById('atas-nama').style.display = 'block';
		}
	}

	$(function () {
		// $("#select-bank-bayar").select2()
		// $("#select-jenis-pembayaran").select2()
		$("#select-sales")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#kode-cabang").val(optionSelected.data("kode"));
					$("#nama-sales").val(optionSelected.data("nama"));
					$("#jabatan").val(optionSelected.data("jabatan"));
					$("#jumlah-barang").val(1);
				});
		$("#select-bank-bayar")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					// $("#nama_bank").val(optionSelected.data("bank"));
					$("#no-rek").val(optionSelected.data("rek"));
					$("#atas-nama").val(optionSelected.data("nama"));
				});
		// $("#btn-simpan").on("click", function(){
		// 	let pembayaran = $("#piutang").val();
		// 	let total = $("#total").val();
		// 	if (pembayaran == "") {
		// 		Swal.fire({
		// 			icon: 'error',
		// 			title: 'Oops...',
		// 			text: 'Belum memasukkan Pembayaran!',
		// 		});
		// 		$("#piutang").focus()
		// 	} else if (pembayaran > total) {
		// 		Swal.fire({
		// 			icon: 'error',
		// 			title: 'Oops...',
		// 			text: 'Pembayaran lebih dari piutang!',
		// 		});
		// 		$("#piutang").val("")
		// 		$("#piutang").focus()
		// 	} else {
		// 		$("#form_detail")
		// 	}
		// });
		$("#btn-print").on("click", function () {
			window.print()
		});
		$('#tampung_harga').mask('000.000.000.000.000.000', {reverse: true});
		$('#bayar').mask('000.000.000.000.000.000', {reverse: true});
	});
</script>
