<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Print Struck</title>
	<style type="text/css">
		html {
			font-family: "Verdana, Arial";
		}

		.content {
			width: 100%;
			font-size: 5px;
			padding: 5px
		}

		.title {
			text-align: center;
			font-size: 13px;
			padding-bottom: 8px;
			border-bottom: 1px solid;
		}

		.head {
			margin-top: 5px;
			margin-bottom: 10px;
			padding-bottom: 10px;
			border-bottom: 1px solid;
		}

		table {
			width: 100%;
			font-size: 10px;
		}

		.thanks {
			/* margin-top: 10px; */
			padding-top: 12px;
			padding-bottom: 8px;
			text-align: center;
			font-size: 10px;
			border-top: 1px solid;
		}

		@media print {
			@page {
				width: 90mm;
				margin: 0mm
			}
		}
	</style>
</head>

<body onload="window.print()">
<div class="content">
	<div class="title">

		<b>CV.Andi Offset</b>
		<br>
		Jl. Beo No.38-40, Mrican, Caturtunggal Yogyakarta 55281
	</div>
	<div class="title">
		<b>Kartu Order</b>
	</div>
	<div class="head">
		<table cellspacing="3" cellpadding="3">
			<tr>
				<td>Tanggal Order : <?= $order->tgl_order ?></td>
				<td>Tanggal Minta Dikirim : <?= $order->tgl_minta_kirim ?></td>
			</tr>
			<tr>
				<td>Nomor Order : <?= $order->no_order ?></td>
				<td>No PO : <?= $order->no_po ?></td>
				<td>No PR : <?= $order->no_pr ?></td>
			</tr>
			<tr>
				<td>Nama Toko : <?= $order->nama_toko ?></td>
			</tr>
			<tr>
				<td>Alamat Toko : <?= $order->alamat_toko ?></td>
			</tr>
			<tr>
				<td>Nama Sales : <?= $order->nama_sales ?></td>
			</tr>
		</table>
	</div>
	<div class="head">
		<table id="example1" class="table table-bordered table-striped">
			<thead>
			<tr>
				<th style="text-align:center">No.</th>
				<th style="text-align:center">Format</th>
				<th style="text-align:center">Jenis Kertas</th>
				<th style="text-align:center">Warna Kertas</th>
				<th style="text-align:center">Warna Tinta</th>
				<th style="text-align:center">Jumlah</th>
			</tr>
			</thead>
			<tbody>
			<?php
			$no = 1;
			foreach ($format as $o) {
				?>
				<tr>
					<td style="text-align:center"><?= $no++ ?></td>
					<td style="text-align:center"><?= $o->format ?></td>
					<td style="text-align:center"><?= $o->jns_kertas ?></td>
					<td style="text-align:center"><?= $o->warna_kertas ?></td>
					<td style="text-align:center"><?= $o->warna_tinta ?></td>
					<td style="text-align:center"><?= $o->jumlah_order ?></td>
				</tr>
				<?php
			}
			?>
			</tbody>
		</table>
	</div>
	<div class="head">
		<table class="transactin-table" cellspacing="2" cellpadding="5">
			<tr>
				<td>Jumlah Order</td>
				<td><?= $order->buku ?> Buku</td>
				<td><?= $order->set_buku ?> Set</td>
				<td><?= $order->eks ?> Eks</td>
			</tr>
		</table>
	</div>
	<div class="head">
		<table cellspacing="2" cellpadding="5">
			<tr>
				<td>Offset/Sablon/Polos : <?= $order->offset_sablon_polos ?></td>
				<td>Perforasi : <?= $order->perforasi ?></td>
			</tr>
			<tr>
				<td>Nomerator : <?= $order->nomerator ?></td>
				<td>Warna Nomerator : <?= $order->warna_nomerator ?></td>
			</tr>
			<tr>
				<td>Bending : <?= $order->bending ?></td>
				<td>UV/Vernish/Laminating/Tidak : <?= $order->uv_vernish_laminating ?></td>
			</tr>
			<tr>
				<td>Foil : <?= $order->foil ?></td>
				<td>Catatan : <?= $order->catatan ?></td>
			</tr>
			<tr>
				<td>Degel : <?= $order->degel ?></td>
			</tr>
		</table>
	</div>
	<div class="thanks">
		------- Terimakasih sudah berberlanja -------
		<br>
		Kepuasan Anda Semangat Kami
	</div>
</div>

</body>

</html>
