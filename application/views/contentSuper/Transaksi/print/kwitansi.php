<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Print Struck Harga</title>
	<style type="text/css">
		html {
			font-family: "Verdana, Arial";
		}

		.content {
			width: 100%;
			font-size: 10px;
			/* padding: 5px; */
			border: 1px solid black;
		}

		.title {
			text-align: center;
			font-size: 10px;
			padding-bottom: 8px;
			border-bottom: 1px solid;
		}

		.head {
			margin-top: 5px;
			margin-bottom: 10px;
			padding-bottom: 10px;
			border-bottom: 1px solid;
		}

		.nota2{

		}

		/* .padding, .nota1, {
		  border: 1px solid black;
		} */
		
		.border-table{
			border: 1px solid black;
		}

		.padding {
		  width: 100%;
		  border-collapse: collapse;
		  font-size: 10px;
		}

		.table {
			width: 100%;
			font-size: 10px;
			
		}

		.noborder{
			border: none !important;
		}

		.thanks {
			/* margin-top: 10px; */
			padding-top: 12px;
			padding-bottom: 8px;
			text-align: center;
			font-size: 10px;
			border-top: 1px solid;
		}

		@media print {
			@page {
				width: 90mm;
				margin: 0mm
			}
		}
	</style>
</head>

<body onload="window.print()">
<div class="content ">
	<div class="title">
    <b>Percetakan dan Penerbitan</b>
		<b>CV.Andi Offset</b>
		<br>
		JL. BEO 38 - 40 TELP (0274) 561881
	</div>
	<div class="title">
		<b>KWITANSI</b>
	</div>
	<div>
		<table cellspacing="3" cellpadding="3">
			<tr>
				<td>Yogyakarta, <?= $kirim->tgl_minta_kirim ?></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;No : <?= $kirim->no_pengantar ?></td>
			</tr>

			<tr>
				<td>Sudah terima dari : <?= $kirim->nama_toko ?><br><br><br><br></td>
			</tr>
			<tr>
				<td>Banyaknya uang : <br></td>
			</tr>
			<tr>
				<td>Untuk Pembayaran :</td>
			</tr>
		</table>
	</div>
	<div>
		<table class="padding">
			<thead>
			<tr>
				<th class="border-table" style="text-align:left">NO KIRIM</th>
				<th class="border-table" style="text-align:left">NO ORDER</th>
				<th class="border-table" style="text-align:left">NO PO</th>
				<th class="border-table" style="text-align:left">NO PR</th>
				<th class="border-table" style="text-align:left">NAMA ORDER</th>
				<th  class="border-table" style="text-align:left">JUMLAH</th>
				<th class="border-table" style="text-align:left">HARGA SATUAN</th>
				<th class="border-table" style="text-align:left">TOTAL HARGA</th>
			</tr>
			</thead>
			<tbody>
			<?php
			foreach ($pengiriman as $o) {
				?>
				<tr>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left">ayam</td>
					<td class="border-table" style="text-align:left"></td>
				</tr>
				<?php
			}
			?>
			<tr>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left "> </td>
				<td class="border-table" style="text-align:left">JUMLAH</td>
				<td class="border-table" style="text-align:left"></td>
			</tr>
			<tr>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left "> </td>
				<td class="border-table" style="text-align:left">PPN</td>
				<td class="border-table" style="text-align:left"></td>
			</tr>
			<tr>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left "> </td>
				<td class="border-table" style="text-align:left">DISKON</td>
				<td class="border-table" style="text-align:left"></td>
			</tr>
			<tr>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left border-bottom-style: none;" class=""> </td>
				<td style="text-align:left "> </td>
				<td class="border-table" style="text-align:left">JUMLAH</td>
				<td class="border-table" style="text-align:left"></td>
			</tr>
			</tbody>
		</table>
	</div>
	<div>
		<table cellspacing="6" cellpadding="6">
		<tr>
				<td>Penerima, </td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Hormat Kami, </td>
			</tr>

			<tr>
				<td>Tanggal, <br><br><br><br></td>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; -->
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pengirim, <br><br><br><br></td>
			</tr>

		</table>
	</div>
</div>

</body>

</html>
