<div class="content-wrapper">
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">

                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <div class="card card-info">
            <div class="card-header">
                <h2>Data Transaksi</h2>
            </div>
            <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="text-align:center">No.</th>
                            <th style="text-align:center">No. Order</th>
                            <th style="text-align:center">Nama Pemesan</th>
                            <th style="text-align:center">Jenis Order</th>
                            <th style="text-align:center">Buku</th>
                            <th style="text-align:center">Set</th>
                            <th style="text-align:center">Eks</th>
                            <th style="width:12%; text-align:center">Harga</th>
<!--                            <th style="text-align:center">Pembayaran</th>-->
                            <th style="width:12%; text-align:center">Total</th>
                            <th style="width:12%; text-align:center">Cicilan</th>
                            <th style="width:12%; text-align:center">Sisa Pembayaran</th>
                            <th style="width:12%; text-align:center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
					<?php
					$no = 1;
					foreach ($orders as $o) {
					?>
                            <tr>
                                <td style="text-align:center"><?= $no++ ?></td>
                                <td style="text-align:center"><?= $o->no_order ?></td>
                                <td style="text-align:center"><?= $o->nama_toko ?></td>
                                <td style="text-align:center"><?= $o->jns_order ?></td>
                                <td style="text-align:center"><?= $o->buku ?></td>
                                <td style="text-align:center"><?= $o->set_buku ?></td>
                                <td style="text-align:center"><?= $o->eks ?></td>
                                <td style="text-align:center"><?= formatRupiah($o->tampung_harga) ?></td>
                                <td style="text-align:center"><?= formatRupiah($o->total_semua) ?></td>
                                <td style="text-align:center"><?= formatRupiah($o->tampung_bayar_1) ?></td>
                                <td style="text-align:center"><?= formatRupiah($o->total) ?></td>
                                <td style="text-align:center">
									<a href="<?= site_url('Transaksi/detail_order/') . $o->nomor ?>"
									   class="btn btn-sm bg-gradient-fuchsia">
										<i class="fas fa-shopping-cart"></i></a>
									<a href="<?= site_url('Transaksi/riwayat_bayar/') . $o->no_order ?>"
									   class="btn btn-sm bg-gradient-fuchsia">
										<i class="fas fa-history"></i></a>
									<a href="<?= site_url('Transaksi/pembayaran/') . $o->nomor ?>"
									   class="btn btn-sm bg-gradient-fuchsia">
										<i class="fas fa-money-check"></i></a>
                                    <a href="<?= site_url('Pengiriman/strukKwitansi/') . $o->nomor ?>"
									   class="btn btn-sm bg-gradient-fuchsia">
										<i class="fas fa-money-bill"></i></a>
                                </td>
                            </tr>
						<?php
					}
					?>
					</tbody>
                </table>
            </div>
        </div>
    </section>
</div>
