<div class="content-wrapper">
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">

				</div>
				<div class="col-sm-6">

				</div>
			</div>
		</div>
	</div>
	<section class="content">
		<div class="card card-info">
			<div class="card-header">
				<h2>Riwayat Transaksi</h2>
			</div>
			<div class="card-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
					<tr>
						<th style="text-align:center">No.</th>
						<th style="text-align:center">Tanggal/Waktu</th>
						<th style="text-align:center">No. Order</th>
						<th style="text-align:center">Nama Pemesan</th>
						<th style="text-align:center">No. PO</th>
						<th style="text-align:center">No. PR</th>
						<th style="text-align:center">Jenis Order</th>
						<th style="text-align:center">Jumlah</th>
						<th style="text-align:center">Harga Satuan</th>
						<th style="text-align:center">Total</th>
						<th style="text-align:center">Jenis Pembayaran</th>
						<th style="text-align:center">No Rek</th>
						<th style="text-align:center">Atas Nama</th>
						<th style="text-align:center">Cicilan</th>
						<th style="text-align:center">Sisa Pembayaran</th>
					</tr>
					</thead>
					<tbody>
					<?php
					$no = 1;
					foreach ($logs as $L) {
						?>
						<tr>
							<td style="text-align:center"><?= $no++ ?></td>
							<td style="text-align:center"><?= $L->log_time ?></td>
							<td style="text-align:center"><?= $L->log_no_order ?></td>
							<td style="text-align:center"><?= $L->log_customer ?></td>
							<td style="text-align:center"><?= $L->log_po ?></td>
							<td style="text-align:center"><?= $L->log_pr ?></td>
							<td style="text-align:center"><?= $L->log_order ?></td>
							<td style="text-align:center"><?= $L->log_jumlah ?></td>
							<td style="text-align:center"><?= $L->log_harga_satuan ?></td>
							<td style="text-align:center"><?= $L->log_total ?></td>
							<td style="text-align:center"><?= $L->log_jns_bayar ?></td>
							<td style="text-align:center"><?= $L->log_no_rek ?></td>
							<td style="text-align:center"><?= $L->log_atas_nama ?></td>
							<td style="text-align:center"><?= formatRupiah($L->log_cicilan) ?></td>
							<td style="text-align:center"><?= formatRupiah($L->log_pembayaran) ?></td>
						</tr>
						<?php
					}
					?>
					</tbody>
				</table>
			</div>
		</div>
	</section>
</div>
