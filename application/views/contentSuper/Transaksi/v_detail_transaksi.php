<div class="content-wrapper">
	<section class="content">
		<form enctype="multipart/form-data" method="post" action="<?= site_url("Transaksi/proses_update"); ?>"
			  role="form">
			<div class="row">
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label for="">No Order</label>
										<input readonly type="text" id="no-order" value="<?= $order->no_order ?>"
											   name="no_order" class="form-control"/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">NO PO</label>
										<input readonly type="number" id="no-po" value="<?= $order->no_po ?>"
											   name="no-po" class="form-control"/>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="">NO PR</label>
										<input readonly type="number" id="no-pr" value="<?= $order->no_pr ?>"
											   name="no-pr" class="form-control"/>
									</div>
								</div>
							</div>
							<!-- <div class="form-group">
								<label for="">Kode Customer</label>
								<input readonly type="text" id="kode-kustomer" value="<?= $order->kode_kustomer ?>"
									   name="kd_kustomer" class="form-control"/>
							</div> -->
							<div class="form-group">
								<label for="">Nama Toko</label>
								<input readonly type="text" id="nama-toko" value="<?= $order->nama_toko ?>"
									   name="nama_toko" class="form-control"/>
							</div>
							<!-- <div class="form-group">
								<label for="">Alamat Toko</label>
								<input readonly type="text" id="alamat-toko" value="<?= $order->alamat_toko ?>"
									   name="alamat_toko" class="form-control"/>
							</div> -->
							<div class="col-md-2">
								<div class="form-group">
									<label for="">Tanggal Order</label>
									<input readonly type="date" id="tanggal-order" value="<?= $order->tgl_order ?>"
										   name="tgl_order" class="form-control"/>
								</div>
								<div class="form-group">
									<label for="">Tanggal Minta Dikirim</label>
									<input readonly type="date" id="tanggal-minta-dikirim"
										   value="<?= $order->tgl_minta_kirim ?>" name="tgl_kirim"
										   class="form-control"/>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-header"><h5>Data Format</h5></div>
						<div class="card-body">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
								<tr>
									<th style="text-align:center">No.</th>
									<th style="text-align:center">Format</th>
									<th style="text-align:center">Jenis Kertas</th>
									<th style="text-align:center">Warna Kertas</th>
									<th style="text-align:center">Warna Tinta</th>
								</tr>
								</thead>
								<tbody>
								<?php
								$no = 1;
								foreach ($formats as $o) {
									?>
									<tr>
										<td style="text-align:center"><?= $no++ ?></td>
										<td style="text-align:center"><?= $o->format ?></td>
										<td style="text-align:center"><?= $o->jns_kertas ?></td>
										<td style="text-align:center"><?= $o->warna_kertas ?></td>
										<td style="text-align:center"><?= $o->warna_tinta ?></td>
									</tr>
									<?php
								}
								?>
							</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Jenis Order</label>
										<input readonly type="text" id="jns_order" value="<?= $order->jns_order ?>"
											   name="jns_order" class="form-control">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Rangkap</label>
										<input readonly type="text" id="rangkap" value="<?= $order->rangkap ?>"
											   name="rangkap" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Buku</label>
										<input readonly type="text" id="buku" value="<?= $order->buku ?>" name="buku"
											   class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Set</label>
										<input readonly type="text" id="set" name="set_buku"
											   value="<?= $order->set_buku ?>" class="form-control"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Eks</label>
										<input readonly type="text" id="eks" name="eks"
											   value="<?= $order->eks ?>" class="form-control"/>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="form-group">
								<label for="">Harga</label>
								<input type="text" min="1" id="harga_order" value="<?= $order->harga_order ?>"
									   name="harga_order" class="form-control"/>
							</div>
							<div class="form-group">
								<label for="">PPN 10%</label>

								<div class="input-group-prepend">
									<input type="number" min="0" id="ppn" name="ppn"
										   class="form-control"/>
									<div class="input-group-prepend">
										<span class="input-group-text">%</span>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="">Diskon</label>
								<div class="input-group-prepend">
									<input type="number" min="0" id="diskon" name="diskon"
										   class="form-control"/>
									<div class="input-group-prepend">
										<span class="input-group-text">%</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<div class="card">
						<div class="card-header"><h5>Spesifikasi</h5></div>
						<div class="card-body">
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Offset/Sablon/Polos</label>
										<input readonly type="text" value="<?= $order->offset_sablon_polos ?>"
											   name="offset_sablon_polos" id="offset_sablon_polos" class="form-control">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Perforasi</label>
										<input readonly type="text" value="<?= $order->perforasi ?>" name="perforasi"
											   id="perforasi" class="form-control">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Nomerator</label>
										<input readonly type="text" id="nomerator" value="<?= $order->nomerator ?>"
											   name="nomerator" class="form-control"/>
									</div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="">Warna Nomerator</label>
										<input readonly type="text" name="warna_nomerator"
											   value="<?= $order->warna_nomerator ?>" class="form-control"
											   id="warna-nomerator">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Bending</label>
										<input readonly type="text" name="bending" value="<?= $order->bending ?>"
											   class="form-control" id="bending">
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">UV/Vernish/Laminating/Tidak</label>
										<input readonly type="text" name="uv_vernish_laminating"
											   value="<?= $order->uv_vernish_laminating ?>" class="form-control"
											   id="uv">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Foil</label><br>
										<input readonly type="text" class="form-control" value="<?= $order->foil ?>"
											   name="foil"/>
									</div>
									<div class="form-group">
										<label for="">Degel</label><br>
										<input readonly type="text" class="form-control" name="degel"
											   value="<?= $order->degel ?>"/>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="">Catatan</label><br>
										<input readonly type="text" class="form-control" name="catatan"
											   value="<?= $order->catatan ?>">
									</div>
								</div>
								<input type="hidden" class="form-control" name="nomor" value="<?= $order->nomor ?>">
							</div>
						</div>
					</div>
				</div>
				
				<div class="card-body">
					<input type="submit" id="btn-simpan" value="Simpan" class="btn btn-primary float-left">
				</div>
				<div class="card-body">
					<a href="<?= site_url('transaksi/print_struk_harga/') . $order->nomor ?>" target="_blank"
					   class="btn btn-primary float-none"><i class="fas fa-print"></i>
						Print Harga
					</a>
					<a href="<?= site_url('transaksi/print_struk/') . $order->nomor ?>" target="_blank"
					   class="btn btn-primary float-right"><i class="fas fa-print"></i>
						Print Laporan Transaksi
					</a>
				</div>
			</div>
		</form>
	</section>
</div>
<!--<script src="https://cdn.jsdelivr.net/npm/gasparesganga-jquery-loading-overlay@2.1.7/dist/loadingoverlay.min.js"></script>-->
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="jquery.masknumber.js"></script>
<script>
	// function cek_bank(obj) {
	// 	var value = obj.value;
	// 	if (value == 1) {
	// 		document.getElementById('no-rek').style.display = 'block';
	// 		document.getElementById('atas-nama').style.display = 'block';
	// 	} else if (value == 2) {
	// 		document.getElementById('no-rek').style.display = 'block';
	// 		document.getElementById('atas-nama').style.display = 'block';
	// 	} else if (value == 3) {
	// 		document.getElementById('no-rek').style.display = 'block';
	// 		document.getElementById('atas-nama').style.display = 'block';
	// 	} else if (value == 4) {
	// 		document.getElementById('no-rek').style.display = 'block';
	// 		document.getElementById('atas-nama').style.display = 'block';
	// 	}
	// }

	function cek_jns_pembayaran(obj) {
		var value = obj.value;
		if (value == "TUNAI") {
			document.getElementById('no-rek').style.display = 'none';
			document.getElementById('atas-nama').style.display = 'none';
		} else {
			document.getElementById('no-rek').style.display = 'block';
			document.getElementById('atas-nama').style.display = 'block';

		}
	}

	$(function () {
		// $("#select-bank-bayar").select2()
		// $("#select-jenis-pembayaran").select2()
		$("#select-sales")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					$("#kode-cabang").val(optionSelected.data("kode"));
					$("#nama-sales").val(optionSelected.data("nama"));
					$("#jabatan").val(optionSelected.data("jabatan"));
					$("#jumlah-barang").val(1);
				});
		$("#select-bank-bayar")
				.select2()
				.on("change", function () {
					var optionSelected = $(this).children("option:selected");
					// $("#nama_bank").val(optionSelected.data("bank"));
					$("#no-rek").val(optionSelected.data("rek"));
					$("#atas-nama").val(optionSelected.data("nama"));
				});
		// $("#btn-simpan").on("click", function(){
		// 	let pembayaran = $("#piutang").val();
		// 	let total = $("#total").val();
		// 	if (pembayaran == "") {
		// 		Swal.fire({
		// 			icon: 'error',
		// 			title: 'Oops...',
		// 			text: 'Belum memasukkan Pembayaran!',
		// 		});
		// 		$("#piutang").focus()
		// 	} else if (pembayaran > total) {
		// 		Swal.fire({
		// 			icon: 'error',
		// 			title: 'Oops...',
		// 			text: 'Pembayaran lebih dari piutang!',
		// 		});
		// 		$("#piutang").val("")
		// 		$("#piutang").focus()
		// 	} else {
		// 		$("#form_detail")
		// 	}
		// });
		$("#btn-print").on("click", function () {
			window.print()
		});
		$('#harga_order').mask('000.000.000.000.000.000', {reverse: true});
		$('#piutang').mask('000.000.000.000.000.000', {reverse: true});
	});
</script>
