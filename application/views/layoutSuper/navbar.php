<nav class="main-header navbar navbar-dark">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>

    </ul>
    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <div class="input-group-append">
                        <a href="<?= site_url('dashboard/logout') ?>" class="nav-link"><i class="fa fa-power-off"></i></a>
            </div>
        </div>
    </form>
</nav>
