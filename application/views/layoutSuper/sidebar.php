<aside class="main-sidebar sidebar-dark-info elevation-4">
	<!-- Brand Logo -->
	<div class="brand-link ">
		<img src="https://2.bp.blogspot.com/-26OOkCpYY7w/XBSmEG3LxSI/AAAAAAAAB_M/Gm7jUOqIBgwbu1eiOAjempNujA7CB48wgCK4BGAYYCw/s400/Lowongan%2BKerja%2BPenerbit%2BAndi%2BKaltim.jpg"
			 alt="AdminLTE Logo" class="center brand-image img-circle elevation-3" style="opacity: .8">
		<span class="brand-text font-weight-light"><?= $this->session->userdata("nama_user") ?></span>
	</div>
	<!-- Sidebar -->
	<div class="sidebar">
		<nav class="mt-2 ">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<?php if ($this->session->userdata("role") == "superadmin") { ?>
					<li class="nav-item has-treeview">
						<a href="<?= site_url('Dashboard') ?>" class="nav-link">
							<i class="fas fa-satellite-dish"></i>
							<p></p>
							<p>
								Dashboard
							</p>
						</a>
					</li>
				<?php } ?>
				<?php if ($this->session->userdata("role") == "superadmin" || $this->session->userdata("role") == "admin") { ?>
					<li class="nav-item" <?= $this->uri->segment(2) == 'customer' ? 'class=" nav-itemactive"' : '' ?>>
						<a href="<?= site_url('customer') ?>" class="nav-link">
							<i class="fas fa-truck-loading"></i>
							<p>|</p>
							<p>Data Customer</p>
						</a>
					</li>
					<li class="nav-item" <?= $this->uri->segment(2) == 'pegawai' ? 'class="nav-item active"' : '' ?>>
						<a href="<?= site_url('pegawai') ?>" class="nav-link">
							<i class="fas fa-address-book"></i>
							<p>|</p>
							<p>Data Pegawai</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<!--					--><?php //if($this->session->userdata("role") == "superadmin" || $this->session->userdata("role") == "admin") { ?>
						<a href="<?= site_url(array("AppSuper")) ?>" class="nav-link">
							<i class="fab fa-android"></i>
							<p>|</p>
							<p>
								<!--							<input type="hidden" name="count_add" id="count_add" pattern="[10]+">-->
								Aplikasi Transaksi
							</p>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="<?= site_url(array("Transaksi")) ?>" class="nav-link">
							<i class="fas fa-money-check"></i>
							<p>|</p>
							<p>
								Data Transaksi
							</p>
							<span class="badge badge-light right"><?= $this->fungsi->HitungTrans(); ?></span>
						</a>
					</li>
					<li class="nav-item has-treeview">
						<a href="<?= site_url(array("Bank")) ?>" class="nav-link">
							<i class="fas fa-money-bill"></i>
							<p>|</p>
							<p>
								Data Bank
							</p>
						</a>
					</li>
				<?php } ?>
				<?php if ($this->session->userdata("role") == "superadmin") { ?>
					<li class="nav-item">
						<a href="<?= site_url('User') ?>" class="nav-link">
							<i class="fas fa-folder-open"></i>
							<p>|</p>
							<p>User Akses</p>
						</a>
					</li>
				<?php } ?>
				<?php if ($this->session->userdata("role") == "superadmin") { ?>
					<li class="nav-item">
						<a href="<?= site_url('Pengiriman') ?>" class="nav-link">
							<i class="fas fa-truck"></i>
							<p>|</p>
							<p>Pengiriman</p>
						</a>
					</li>
				<?php } ?>
				<?php if ($this->session->userdata("role") == "superadmin") { ?>
					<li class="nav-item">
						<a href="<?= site_url('Laporan') ?>" class="nav-link">
							<i class="fas fa-file"></i>
							<p>|</p>
							<p>Laporan</p>
						</a>
					</li>
				<?php } ?>
			</ul>
			<br>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>
