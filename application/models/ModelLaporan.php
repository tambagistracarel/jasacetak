<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ModelLaporan extends CI_Model {

    public function order_pertanggal(){
        $this->db->select('*');
        $this->db->from('kartu_order');
        // $this->db->join('kustomer', 'kartu_order.kode_kustomer = customer.kode_kustomer', 'left');
        // $this->db->order_by('id_pemesan', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }

    public function per_pemesan(){
        $this->db->select('*');
        $this->db->from('kartu_order');
        $this->db->group_by('nama_toko');
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function detail_per_pemesan($post){
        $this->db->select('*');
        $this->db->from('kartu_order');
        $this->db->where('nama_toko ', $post['tangkap_nama']);
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function pembayaran_per_pemesan(){
        $this->db->select('*');
        $this->db->from('tabel_log');
        $this->db->group_by('log_customer');
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function detail_pembayaran_per_pemesan($post){
        $this->db->select('*');
        $this->db->from('tabel_log');
        $this->db->where('log_customer ', $post['tangkap_pembayaran_per_pemesan']);
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function pembayaran_pertanggal(){
        $this->db->select('*');
        $this->db->from('tabel_log');
        // $this->db->join('kustomer', 'kartu_order.kode_kustomer = customer.kode_kustomer', 'left');
        // $this->db->order_by('id_pemesan', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }

    // public function filter_pembayaran_pertanggal($pecah_tanggal){
    //     $this->db->select('*')
    //             ->from('kartu_order')
    //             ->where('tgl_minta_kirim >=', $pecah_tanggal[0])
    //             ->where('tgl_minta_kirim <=', $pecah_tanggal[1]);
    //     $query = $this->db->get_compiled_select();
    //     // print('<pre>');print_r($query);exit();
    //     $data = $this->db->query($query)->result();
    //     // $_SESSION["isi_tanggal"] = "isi";
    //     return $data;
    // }

    public function detail_pengiriman_per_pemesan($post){
        $this->db->select('*');
        $this->db->from('kartu_order');
        $this->db->where('nama_toko ', $post['tangkap_pengiriman_per_pemesan']);
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function detail_pengiriman_per_no_order($post){
        $this->db->select('*');
        $this->db->from('kartu_order');
        $this->db->where('no_order', $post['tangkap_pengiriman_no_order']);
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

    public function pengiriman_pertanggal(){
        $this->db->select('*');
        $this->db->from('kartu_order');
        // $this->db->join('kustomer', 'kartu_order.kode_kustomer = customer.kode_kustomer', 'left');
        // $this->db->order_by('id_pemesan', 'desc');
        $query = $this->db->get()->result();
        return $query;
    }    

    public function filter_pengiriman_pertanggal($pecah_tanggal){
        $this->db->select('*')
                ->from('kartu_order')
                ->where('tgl_minta_kirim >=', $pecah_tanggal[0])
                ->where('tgl_minta_kirim <=', $pecah_tanggal[1]);
        $query = $this->db->get_compiled_select();
        // print('<pre>');print_r($query);exit();
        $data = $this->db->query($query)->result();
        // $_SESSION["isi_tanggal"] = "isi";
        return $data;
    }

    public function pengiriman_no_order(){
        $this->db->select('*');
        $this->db->from('kartu_order');
        // $this->db->where('nama_toko ', $post['tangkap_nama']);
        $query = $this->db->get_compiled_select();
        $data = $this->db->query($query)->result();
        return $data;
    }

}

/* End of file ModelLaporan.php */
/* Location: ./application/models/ModelLaporan.php */