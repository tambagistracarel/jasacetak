<?php


class ModelOrder extends CI_Model
{
	var $table = "kartu_order";
	var $primaryKey = "nomor";

	public function insert($data)
	{
		// $this->db->set($data);
		return $this->db->insert($this->table, $data);
	}

	public function insertGetId($data) {
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}

	public function totTrans()
	{
		return $this->db->get($this->table);
	}

	public function getAll()
	{
		return $this->db->get($this->table)->result();
	}

	public function getByPrimaryKey($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->get($this->table)->row();
	}


	public function update($id, $data)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->update($this->table, $data);
	}

	public function update2($data)
	{
		$this->db->update($this->table, $data);
	}

	public function getSubTotalById($no){
		$query = "select total from kartu_order where no_order = '$no'";
		$result =  $this->db->query($query);
		return $result->row()->total;
	}

	public function getNomor($no){
		$query = "select * from kartu_order where nomor = '$no'";
		$result =  $this->db->query($query);
		return $result->row()->total;
	}

	public function getSubBayarById($noOrder)
	{
		$query = "select tampung_bayar_1 from kartu_order where no_order = '$noOrder'";
		$result =  $this->db->query($query);
		return $result->row()->tampung_bayar_1;
//		return $this->db->get('tabel_log')->result();
	}

	public function delete($id)
	{
		$this->db->where($this->primaryKey, $id);
		return $this->db->delete($this->table);
	}

	public function getPemesan(){  
        $this->db->select('nama_toko, alamat_toko');
        $this->db->from('kartu_order');
        // $this->db->where("nama_toko = '$nama_toko'");
        $this->db->group_by('nama_toko'); 
        $query = $this->db->get(); 
		return $query->result();
		// SELECT nama_toko FROM kartu_order WHERE nama_toko = 'Arman Hakim Nasution, Ir.,M.Eng' GROUP BY nama_toko
    }
}
